<%@ include file="/init.jsp" %>

<%@page import="java.util.LinkedHashMap"%>
<%@page import="com.liferay.portal.kernel.exception.SystemException"%>
<%@page import="com.liferay.portal.kernel.service.UserLocalServiceUtil"%>
<%@page import="com.liferay.portal.kernel.model.User"%>
<%@page import="java.util.List"%>
<%@page import="com.liferay.portal.kernel.util.ListUtil"%>
<%@ include file="/META-INF/resources/init.jsp" %>

<portlet:actionURL name="getUser" var="actionUrl" > </portlet:actionURL>
 
<div class="container">
<aui:form name="fm" action="<%=actionUrl%>" method="post">
	<div class="col-sm-3 col-md-3">
	<aui:input type="text" label="StartDate" name="startDate" /> 
	</div>
	<div class="col-sm-3 col-md-3">
	<aui:input type="text" label="EndDate" name="endDate" /> 
	</div>	
<aui:button type="submit" value="Enter" />
</aui:form>

</div>


<%
List<User> userList = (List<User>)request.getAttribute("LRUser");

System.out.println("userList ........ " +userList);


if(userList != null){
%>


<liferay-ui:search-container delta="4"
	emptyResultsMessage="Sorry. There are no items to display.">
   <liferay-ui:search-container-results  results="<%= ListUtil.subList(userList, searchContainer.getStart(), searchContainer.getEnd()) %>" />
  		<liferay-ui:search-container-row className="com.liferay.portal.kernel.model.User"  modelVar="record">
  		     <liferay-ui:search-container-column-text name="User Id" property="userId" />
            <liferay-ui:search-container-column-text name="First Name" property="firstName" />
            <liferay-ui:search-container-column-text name="LastName" property="lastName" />
            <liferay-ui:search-container-column-text name="Designation" property="jobTitle" />
        
   		</liferay-ui:search-container-row>    
   		<liferay-ui:search-iterator />
</liferay-ui:search-container>

<%}%>


<aui:script>
	AUI().use('aui-datepicker', function(A) {
		var a = new A.DatePicker({
			trigger : '#<portlet:namespace/>startDate',

			popover : {
				zIndex : 1
			}
		});

		new A.DatePicker({
			trigger : '#<portlet:namespace/>endDate',
			popover : {
				zIndex : 1
			}
		});
	});
</aui:script>