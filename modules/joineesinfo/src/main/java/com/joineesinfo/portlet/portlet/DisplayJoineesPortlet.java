package com.joineesinfo.portlet.portlet;

import com.joineesinfo.portlet.constants.DisplayJoineesPortletKeys;
import com.liferay.portal.kernel.dao.orm.DynamicQuery;
import com.liferay.portal.kernel.dao.orm.DynamicQueryFactoryUtil;
import com.liferay.portal.kernel.dao.orm.PropertyFactoryUtil;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.model.User;
import com.liferay.portal.kernel.portlet.bridges.mvc.MVCPortlet;
import com.liferay.portal.kernel.service.UserLocalServiceUtil;
import com.liferay.portal.kernel.util.ParamUtil;
import com.liferay.portal.kernel.util.PortalClassLoaderUtil;

import java.util.Date;
import java.util.List;

import javax.portlet.ActionRequest;
import javax.portlet.ActionResponse;
import javax.portlet.Portlet;

import org.osgi.service.component.annotations.Component;

/**
 * @author E2 LR6
 */
@Component(
	immediate = true,
	property = {
		"com.liferay.portlet.display-category=category.sample",
		"com.liferay.portlet.instanceable=true",
		"javax.portlet.display-name=joineesinfo Portlet",
		"javax.portlet.init-param.template-path=/",
		"javax.portlet.init-param.view-template=/view.jsp",
		"javax.portlet.name=" + DisplayJoineesPortletKeys.DisplayJoinees,
		"javax.portlet.resource-bundle=content.Language",
		"javax.portlet.security-role-ref=power-user,user"
	},
	service = Portlet.class
)
public class DisplayJoineesPortlet extends MVCPortlet {
	
	public void getUser(ActionRequest request , ActionResponse response){
		String startDate = ParamUtil.getString(request, "startDate");	
		String endDate = ParamUtil.getString(request, "endDate");
		
		
		System.out.println(startDate);
		System.out.println(endDate);
		
		DynamicQuery dynamicQuery = DynamicQueryFactoryUtil.forClass(User.class, PortalClassLoaderUtil.getClassLoader());
		dynamicQuery.add(PropertyFactoryUtil.forName("createDate").between(new Date(startDate), new Date(endDate)));	
		
		List<User> userList = UserLocalServiceUtil.dynamicQuery(dynamicQuery);
		System.out.println("userList 8888  " +userList);
		request.setAttribute("LRUser", userList);
		
		
		try {
			
			for (User user : userList) {
				System.out.println("User Id==>"+user.getUserId()+" Name==>"+user.getFirstName() + "Designation ==>"+user.getJobTitle());
			}
			
		} catch (SystemException e) {
			e.printStackTrace();
		}
		
	    
		}
}