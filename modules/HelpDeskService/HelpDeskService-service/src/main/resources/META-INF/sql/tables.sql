create table serveReq (
	serveReqID LONG not null primary key,
	issueCategory VARCHAR(75) null,
	issueTitle VARCHAR(75) null,
	issueDesc VARCHAR(75) null,
	issueImpact VARCHAR(75) null,
	issueUrgency VARCHAR(75) null
);