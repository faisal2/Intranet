/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package com.HelpDesk.model.impl;

import aQute.bnd.annotation.ProviderType;

import com.HelpDesk.model.serviceReq;

import com.liferay.portal.kernel.model.CacheModel;
import com.liferay.portal.kernel.util.HashUtil;
import com.liferay.portal.kernel.util.StringBundler;
import com.liferay.portal.kernel.util.StringPool;

import java.io.Externalizable;
import java.io.IOException;
import java.io.ObjectInput;
import java.io.ObjectOutput;

/**
 * The cache model class for representing serviceReq in entity cache.
 *
 * @author Brian Wing Shun Chan
 * @see serviceReq
 * @generated
 */
@ProviderType
public class serviceReqCacheModel implements CacheModel<serviceReq>,
	Externalizable {
	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}

		if (!(obj instanceof serviceReqCacheModel)) {
			return false;
		}

		serviceReqCacheModel serviceReqCacheModel = (serviceReqCacheModel)obj;

		if (serveReqID == serviceReqCacheModel.serveReqID) {
			return true;
		}

		return false;
	}

	@Override
	public int hashCode() {
		return HashUtil.hash(0, serveReqID);
	}

	@Override
	public String toString() {
		StringBundler sb = new StringBundler(13);

		sb.append("{serveReqID=");
		sb.append(serveReqID);
		sb.append(", issueCategory=");
		sb.append(issueCategory);
		sb.append(", issueTitle=");
		sb.append(issueTitle);
		sb.append(", issueDesc=");
		sb.append(issueDesc);
		sb.append(", issueImpact=");
		sb.append(issueImpact);
		sb.append(", issueUrgency=");
		sb.append(issueUrgency);
		sb.append("}");

		return sb.toString();
	}

	@Override
	public serviceReq toEntityModel() {
		serviceReqImpl serviceReqImpl = new serviceReqImpl();

		serviceReqImpl.setServeReqID(serveReqID);

		if (issueCategory == null) {
			serviceReqImpl.setIssueCategory(StringPool.BLANK);
		}
		else {
			serviceReqImpl.setIssueCategory(issueCategory);
		}

		if (issueTitle == null) {
			serviceReqImpl.setIssueTitle(StringPool.BLANK);
		}
		else {
			serviceReqImpl.setIssueTitle(issueTitle);
		}

		if (issueDesc == null) {
			serviceReqImpl.setIssueDesc(StringPool.BLANK);
		}
		else {
			serviceReqImpl.setIssueDesc(issueDesc);
		}

		if (issueImpact == null) {
			serviceReqImpl.setIssueImpact(StringPool.BLANK);
		}
		else {
			serviceReqImpl.setIssueImpact(issueImpact);
		}

		if (issueUrgency == null) {
			serviceReqImpl.setIssueUrgency(StringPool.BLANK);
		}
		else {
			serviceReqImpl.setIssueUrgency(issueUrgency);
		}

		serviceReqImpl.resetOriginalValues();

		return serviceReqImpl;
	}

	@Override
	public void readExternal(ObjectInput objectInput) throws IOException {
		serveReqID = objectInput.readLong();
		issueCategory = objectInput.readUTF();
		issueTitle = objectInput.readUTF();
		issueDesc = objectInput.readUTF();
		issueImpact = objectInput.readUTF();
		issueUrgency = objectInput.readUTF();
	}

	@Override
	public void writeExternal(ObjectOutput objectOutput)
		throws IOException {
		objectOutput.writeLong(serveReqID);

		if (issueCategory == null) {
			objectOutput.writeUTF(StringPool.BLANK);
		}
		else {
			objectOutput.writeUTF(issueCategory);
		}

		if (issueTitle == null) {
			objectOutput.writeUTF(StringPool.BLANK);
		}
		else {
			objectOutput.writeUTF(issueTitle);
		}

		if (issueDesc == null) {
			objectOutput.writeUTF(StringPool.BLANK);
		}
		else {
			objectOutput.writeUTF(issueDesc);
		}

		if (issueImpact == null) {
			objectOutput.writeUTF(StringPool.BLANK);
		}
		else {
			objectOutput.writeUTF(issueImpact);
		}

		if (issueUrgency == null) {
			objectOutput.writeUTF(StringPool.BLANK);
		}
		else {
			objectOutput.writeUTF(issueUrgency);
		}
	}

	public long serveReqID;
	public String issueCategory;
	public String issueTitle;
	public String issueDesc;
	public String issueImpact;
	public String issueUrgency;
}