/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package com.HelpDesk.model.impl;

import aQute.bnd.annotation.ProviderType;

import com.HelpDesk.model.serviceReq;
import com.HelpDesk.model.serviceReqModel;

import com.liferay.expando.kernel.model.ExpandoBridge;
import com.liferay.expando.kernel.util.ExpandoBridgeFactoryUtil;

import com.liferay.portal.kernel.bean.AutoEscapeBeanHandler;
import com.liferay.portal.kernel.model.CacheModel;
import com.liferay.portal.kernel.model.impl.BaseModelImpl;
import com.liferay.portal.kernel.service.ServiceContext;
import com.liferay.portal.kernel.util.GetterUtil;
import com.liferay.portal.kernel.util.ProxyUtil;
import com.liferay.portal.kernel.util.StringBundler;
import com.liferay.portal.kernel.util.StringPool;

import java.io.Serializable;

import java.sql.Types;

import java.util.HashMap;
import java.util.Map;

/**
 * The base model implementation for the serviceReq service. Represents a row in the &quot;serveReq&quot; database table, with each column mapped to a property of this class.
 *
 * <p>
 * This implementation and its corresponding interface {@link serviceReqModel} exist only as a container for the default property accessors generated by ServiceBuilder. Helper methods and all application logic should be put in {@link serviceReqImpl}.
 * </p>
 *
 * @author Brian Wing Shun Chan
 * @see serviceReqImpl
 * @see serviceReq
 * @see serviceReqModel
 * @generated
 */
@ProviderType
public class serviceReqModelImpl extends BaseModelImpl<serviceReq>
	implements serviceReqModel {
	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never modify or reference this class directly. All methods that expect a service req model instance should use the {@link serviceReq} interface instead.
	 */
	public static final String TABLE_NAME = "serveReq";
	public static final Object[][] TABLE_COLUMNS = {
			{ "serveReqID", Types.BIGINT },
			{ "issueCategory", Types.VARCHAR },
			{ "issueTitle", Types.VARCHAR },
			{ "issueDesc", Types.VARCHAR },
			{ "issueImpact", Types.VARCHAR },
			{ "issueUrgency", Types.VARCHAR }
		};
	public static final Map<String, Integer> TABLE_COLUMNS_MAP = new HashMap<String, Integer>();

	static {
		TABLE_COLUMNS_MAP.put("serveReqID", Types.BIGINT);
		TABLE_COLUMNS_MAP.put("issueCategory", Types.VARCHAR);
		TABLE_COLUMNS_MAP.put("issueTitle", Types.VARCHAR);
		TABLE_COLUMNS_MAP.put("issueDesc", Types.VARCHAR);
		TABLE_COLUMNS_MAP.put("issueImpact", Types.VARCHAR);
		TABLE_COLUMNS_MAP.put("issueUrgency", Types.VARCHAR);
	}

	public static final String TABLE_SQL_CREATE = "create table serveReq (serveReqID LONG not null primary key,issueCategory VARCHAR(75) null,issueTitle VARCHAR(75) null,issueDesc VARCHAR(75) null,issueImpact VARCHAR(75) null,issueUrgency VARCHAR(75) null)";
	public static final String TABLE_SQL_DROP = "drop table serveReq";
	public static final String ORDER_BY_JPQL = " ORDER BY serviceReq.serveReqID ASC";
	public static final String ORDER_BY_SQL = " ORDER BY serveReq.serveReqID ASC";
	public static final String DATA_SOURCE = "liferayDataSource";
	public static final String SESSION_FACTORY = "liferaySessionFactory";
	public static final String TX_MANAGER = "liferayTransactionManager";
	public static final boolean ENTITY_CACHE_ENABLED = GetterUtil.getBoolean(com.HelpDesk.service.util.ServiceProps.get(
				"value.object.entity.cache.enabled.com.HelpDesk.model.serviceReq"),
			true);
	public static final boolean FINDER_CACHE_ENABLED = GetterUtil.getBoolean(com.HelpDesk.service.util.ServiceProps.get(
				"value.object.finder.cache.enabled.com.HelpDesk.model.serviceReq"),
			true);
	public static final boolean COLUMN_BITMASK_ENABLED = false;
	public static final long LOCK_EXPIRATION_TIME = GetterUtil.getLong(com.HelpDesk.service.util.ServiceProps.get(
				"lock.expiration.time.com.HelpDesk.model.serviceReq"));

	public serviceReqModelImpl() {
	}

	@Override
	public long getPrimaryKey() {
		return _serveReqID;
	}

	@Override
	public void setPrimaryKey(long primaryKey) {
		setServeReqID(primaryKey);
	}

	@Override
	public Serializable getPrimaryKeyObj() {
		return _serveReqID;
	}

	@Override
	public void setPrimaryKeyObj(Serializable primaryKeyObj) {
		setPrimaryKey(((Long)primaryKeyObj).longValue());
	}

	@Override
	public Class<?> getModelClass() {
		return serviceReq.class;
	}

	@Override
	public String getModelClassName() {
		return serviceReq.class.getName();
	}

	@Override
	public Map<String, Object> getModelAttributes() {
		Map<String, Object> attributes = new HashMap<String, Object>();

		attributes.put("serveReqID", getServeReqID());
		attributes.put("issueCategory", getIssueCategory());
		attributes.put("issueTitle", getIssueTitle());
		attributes.put("issueDesc", getIssueDesc());
		attributes.put("issueImpact", getIssueImpact());
		attributes.put("issueUrgency", getIssueUrgency());

		attributes.put("entityCacheEnabled", isEntityCacheEnabled());
		attributes.put("finderCacheEnabled", isFinderCacheEnabled());

		return attributes;
	}

	@Override
	public void setModelAttributes(Map<String, Object> attributes) {
		Long serveReqID = (Long)attributes.get("serveReqID");

		if (serveReqID != null) {
			setServeReqID(serveReqID);
		}

		String issueCategory = (String)attributes.get("issueCategory");

		if (issueCategory != null) {
			setIssueCategory(issueCategory);
		}

		String issueTitle = (String)attributes.get("issueTitle");

		if (issueTitle != null) {
			setIssueTitle(issueTitle);
		}

		String issueDesc = (String)attributes.get("issueDesc");

		if (issueDesc != null) {
			setIssueDesc(issueDesc);
		}

		String issueImpact = (String)attributes.get("issueImpact");

		if (issueImpact != null) {
			setIssueImpact(issueImpact);
		}

		String issueUrgency = (String)attributes.get("issueUrgency");

		if (issueUrgency != null) {
			setIssueUrgency(issueUrgency);
		}
	}

	@Override
	public long getServeReqID() {
		return _serveReqID;
	}

	@Override
	public void setServeReqID(long serveReqID) {
		_serveReqID = serveReqID;
	}

	@Override
	public String getIssueCategory() {
		if (_issueCategory == null) {
			return StringPool.BLANK;
		}
		else {
			return _issueCategory;
		}
	}

	@Override
	public void setIssueCategory(String issueCategory) {
		_issueCategory = issueCategory;
	}

	@Override
	public String getIssueTitle() {
		if (_issueTitle == null) {
			return StringPool.BLANK;
		}
		else {
			return _issueTitle;
		}
	}

	@Override
	public void setIssueTitle(String issueTitle) {
		_issueTitle = issueTitle;
	}

	@Override
	public String getIssueDesc() {
		if (_issueDesc == null) {
			return StringPool.BLANK;
		}
		else {
			return _issueDesc;
		}
	}

	@Override
	public void setIssueDesc(String issueDesc) {
		_issueDesc = issueDesc;
	}

	@Override
	public String getIssueImpact() {
		if (_issueImpact == null) {
			return StringPool.BLANK;
		}
		else {
			return _issueImpact;
		}
	}

	@Override
	public void setIssueImpact(String issueImpact) {
		_issueImpact = issueImpact;
	}

	@Override
	public String getIssueUrgency() {
		if (_issueUrgency == null) {
			return StringPool.BLANK;
		}
		else {
			return _issueUrgency;
		}
	}

	@Override
	public void setIssueUrgency(String issueUrgency) {
		_issueUrgency = issueUrgency;
	}

	@Override
	public ExpandoBridge getExpandoBridge() {
		return ExpandoBridgeFactoryUtil.getExpandoBridge(0,
			serviceReq.class.getName(), getPrimaryKey());
	}

	@Override
	public void setExpandoBridgeAttributes(ServiceContext serviceContext) {
		ExpandoBridge expandoBridge = getExpandoBridge();

		expandoBridge.setAttributes(serviceContext);
	}

	@Override
	public serviceReq toEscapedModel() {
		if (_escapedModel == null) {
			_escapedModel = (serviceReq)ProxyUtil.newProxyInstance(_classLoader,
					_escapedModelInterfaces, new AutoEscapeBeanHandler(this));
		}

		return _escapedModel;
	}

	@Override
	public Object clone() {
		serviceReqImpl serviceReqImpl = new serviceReqImpl();

		serviceReqImpl.setServeReqID(getServeReqID());
		serviceReqImpl.setIssueCategory(getIssueCategory());
		serviceReqImpl.setIssueTitle(getIssueTitle());
		serviceReqImpl.setIssueDesc(getIssueDesc());
		serviceReqImpl.setIssueImpact(getIssueImpact());
		serviceReqImpl.setIssueUrgency(getIssueUrgency());

		serviceReqImpl.resetOriginalValues();

		return serviceReqImpl;
	}

	@Override
	public int compareTo(serviceReq serviceReq) {
		long primaryKey = serviceReq.getPrimaryKey();

		if (getPrimaryKey() < primaryKey) {
			return -1;
		}
		else if (getPrimaryKey() > primaryKey) {
			return 1;
		}
		else {
			return 0;
		}
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}

		if (!(obj instanceof serviceReq)) {
			return false;
		}

		serviceReq serviceReq = (serviceReq)obj;

		long primaryKey = serviceReq.getPrimaryKey();

		if (getPrimaryKey() == primaryKey) {
			return true;
		}
		else {
			return false;
		}
	}

	@Override
	public int hashCode() {
		return (int)getPrimaryKey();
	}

	@Override
	public boolean isEntityCacheEnabled() {
		return ENTITY_CACHE_ENABLED;
	}

	@Override
	public boolean isFinderCacheEnabled() {
		return FINDER_CACHE_ENABLED;
	}

	@Override
	public void resetOriginalValues() {
	}

	@Override
	public CacheModel<serviceReq> toCacheModel() {
		serviceReqCacheModel serviceReqCacheModel = new serviceReqCacheModel();

		serviceReqCacheModel.serveReqID = getServeReqID();

		serviceReqCacheModel.issueCategory = getIssueCategory();

		String issueCategory = serviceReqCacheModel.issueCategory;

		if ((issueCategory != null) && (issueCategory.length() == 0)) {
			serviceReqCacheModel.issueCategory = null;
		}

		serviceReqCacheModel.issueTitle = getIssueTitle();

		String issueTitle = serviceReqCacheModel.issueTitle;

		if ((issueTitle != null) && (issueTitle.length() == 0)) {
			serviceReqCacheModel.issueTitle = null;
		}

		serviceReqCacheModel.issueDesc = getIssueDesc();

		String issueDesc = serviceReqCacheModel.issueDesc;

		if ((issueDesc != null) && (issueDesc.length() == 0)) {
			serviceReqCacheModel.issueDesc = null;
		}

		serviceReqCacheModel.issueImpact = getIssueImpact();

		String issueImpact = serviceReqCacheModel.issueImpact;

		if ((issueImpact != null) && (issueImpact.length() == 0)) {
			serviceReqCacheModel.issueImpact = null;
		}

		serviceReqCacheModel.issueUrgency = getIssueUrgency();

		String issueUrgency = serviceReqCacheModel.issueUrgency;

		if ((issueUrgency != null) && (issueUrgency.length() == 0)) {
			serviceReqCacheModel.issueUrgency = null;
		}

		return serviceReqCacheModel;
	}

	@Override
	public String toString() {
		StringBundler sb = new StringBundler(13);

		sb.append("{serveReqID=");
		sb.append(getServeReqID());
		sb.append(", issueCategory=");
		sb.append(getIssueCategory());
		sb.append(", issueTitle=");
		sb.append(getIssueTitle());
		sb.append(", issueDesc=");
		sb.append(getIssueDesc());
		sb.append(", issueImpact=");
		sb.append(getIssueImpact());
		sb.append(", issueUrgency=");
		sb.append(getIssueUrgency());
		sb.append("}");

		return sb.toString();
	}

	@Override
	public String toXmlString() {
		StringBundler sb = new StringBundler(22);

		sb.append("<model><model-name>");
		sb.append("com.HelpDesk.model.serviceReq");
		sb.append("</model-name>");

		sb.append(
			"<column><column-name>serveReqID</column-name><column-value><![CDATA[");
		sb.append(getServeReqID());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>issueCategory</column-name><column-value><![CDATA[");
		sb.append(getIssueCategory());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>issueTitle</column-name><column-value><![CDATA[");
		sb.append(getIssueTitle());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>issueDesc</column-name><column-value><![CDATA[");
		sb.append(getIssueDesc());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>issueImpact</column-name><column-value><![CDATA[");
		sb.append(getIssueImpact());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>issueUrgency</column-name><column-value><![CDATA[");
		sb.append(getIssueUrgency());
		sb.append("]]></column-value></column>");

		sb.append("</model>");

		return sb.toString();
	}

	private static final ClassLoader _classLoader = serviceReq.class.getClassLoader();
	private static final Class<?>[] _escapedModelInterfaces = new Class[] {
			serviceReq.class
		};
	private long _serveReqID;
	private String _issueCategory;
	private String _issueTitle;
	private String _issueDesc;
	private String _issueImpact;
	private String _issueUrgency;
	private serviceReq _escapedModel;
}