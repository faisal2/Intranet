/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package com.HelpDesk.service.persistence.impl;

import aQute.bnd.annotation.ProviderType;

import com.HelpDesk.exception.NoSuchserviceReqException;

import com.HelpDesk.model.impl.serviceReqImpl;
import com.HelpDesk.model.impl.serviceReqModelImpl;
import com.HelpDesk.model.serviceReq;

import com.HelpDesk.service.persistence.serviceReqPersistence;

import com.liferay.portal.kernel.dao.orm.EntityCache;
import com.liferay.portal.kernel.dao.orm.FinderCache;
import com.liferay.portal.kernel.dao.orm.FinderPath;
import com.liferay.portal.kernel.dao.orm.Query;
import com.liferay.portal.kernel.dao.orm.QueryUtil;
import com.liferay.portal.kernel.dao.orm.Session;
import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.service.persistence.impl.BasePersistenceImpl;
import com.liferay.portal.kernel.util.OrderByComparator;
import com.liferay.portal.kernel.util.StringBundler;
import com.liferay.portal.kernel.util.StringPool;
import com.liferay.portal.spring.extender.service.ServiceReference;

import java.io.Serializable;

import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;

/**
 * The persistence implementation for the service req service.
 *
 * <p>
 * Caching information and settings can be found in <code>portal.properties</code>
 * </p>
 *
 * @author Brian Wing Shun Chan
 * @see serviceReqPersistence
 * @see com.HelpDesk.service.persistence.serviceReqUtil
 * @generated
 */
@ProviderType
public class serviceReqPersistenceImpl extends BasePersistenceImpl<serviceReq>
	implements serviceReqPersistence {
	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never modify or reference this class directly. Always use {@link serviceReqUtil} to access the service req persistence. Modify <code>service.xml</code> and rerun ServiceBuilder to regenerate this class.
	 */
	public static final String FINDER_CLASS_NAME_ENTITY = serviceReqImpl.class.getName();
	public static final String FINDER_CLASS_NAME_LIST_WITH_PAGINATION = FINDER_CLASS_NAME_ENTITY +
		".List1";
	public static final String FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION = FINDER_CLASS_NAME_ENTITY +
		".List2";
	public static final FinderPath FINDER_PATH_WITH_PAGINATION_FIND_ALL = new FinderPath(serviceReqModelImpl.ENTITY_CACHE_ENABLED,
			serviceReqModelImpl.FINDER_CACHE_ENABLED, serviceReqImpl.class,
			FINDER_CLASS_NAME_LIST_WITH_PAGINATION, "findAll", new String[0]);
	public static final FinderPath FINDER_PATH_WITHOUT_PAGINATION_FIND_ALL = new FinderPath(serviceReqModelImpl.ENTITY_CACHE_ENABLED,
			serviceReqModelImpl.FINDER_CACHE_ENABLED, serviceReqImpl.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "findAll", new String[0]);
	public static final FinderPath FINDER_PATH_COUNT_ALL = new FinderPath(serviceReqModelImpl.ENTITY_CACHE_ENABLED,
			serviceReqModelImpl.FINDER_CACHE_ENABLED, Long.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "countAll", new String[0]);

	public serviceReqPersistenceImpl() {
		setModelClass(serviceReq.class);
	}

	/**
	 * Caches the service req in the entity cache if it is enabled.
	 *
	 * @param serviceReq the service req
	 */
	@Override
	public void cacheResult(serviceReq serviceReq) {
		entityCache.putResult(serviceReqModelImpl.ENTITY_CACHE_ENABLED,
			serviceReqImpl.class, serviceReq.getPrimaryKey(), serviceReq);

		serviceReq.resetOriginalValues();
	}

	/**
	 * Caches the service reqs in the entity cache if it is enabled.
	 *
	 * @param serviceReqs the service reqs
	 */
	@Override
	public void cacheResult(List<serviceReq> serviceReqs) {
		for (serviceReq serviceReq : serviceReqs) {
			if (entityCache.getResult(
						serviceReqModelImpl.ENTITY_CACHE_ENABLED,
						serviceReqImpl.class, serviceReq.getPrimaryKey()) == null) {
				cacheResult(serviceReq);
			}
			else {
				serviceReq.resetOriginalValues();
			}
		}
	}

	/**
	 * Clears the cache for all service reqs.
	 *
	 * <p>
	 * The {@link EntityCache} and {@link FinderCache} are both cleared by this method.
	 * </p>
	 */
	@Override
	public void clearCache() {
		entityCache.clearCache(serviceReqImpl.class);

		finderCache.clearCache(FINDER_CLASS_NAME_ENTITY);
		finderCache.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
		finderCache.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);
	}

	/**
	 * Clears the cache for the service req.
	 *
	 * <p>
	 * The {@link EntityCache} and {@link FinderCache} are both cleared by this method.
	 * </p>
	 */
	@Override
	public void clearCache(serviceReq serviceReq) {
		entityCache.removeResult(serviceReqModelImpl.ENTITY_CACHE_ENABLED,
			serviceReqImpl.class, serviceReq.getPrimaryKey());

		finderCache.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
		finderCache.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);
	}

	@Override
	public void clearCache(List<serviceReq> serviceReqs) {
		finderCache.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
		finderCache.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);

		for (serviceReq serviceReq : serviceReqs) {
			entityCache.removeResult(serviceReqModelImpl.ENTITY_CACHE_ENABLED,
				serviceReqImpl.class, serviceReq.getPrimaryKey());
		}
	}

	/**
	 * Creates a new service req with the primary key. Does not add the service req to the database.
	 *
	 * @param serveReqID the primary key for the new service req
	 * @return the new service req
	 */
	@Override
	public serviceReq create(long serveReqID) {
		serviceReq serviceReq = new serviceReqImpl();

		serviceReq.setNew(true);
		serviceReq.setPrimaryKey(serveReqID);

		return serviceReq;
	}

	/**
	 * Removes the service req with the primary key from the database. Also notifies the appropriate model listeners.
	 *
	 * @param serveReqID the primary key of the service req
	 * @return the service req that was removed
	 * @throws NoSuchserviceReqException if a service req with the primary key could not be found
	 */
	@Override
	public serviceReq remove(long serveReqID) throws NoSuchserviceReqException {
		return remove((Serializable)serveReqID);
	}

	/**
	 * Removes the service req with the primary key from the database. Also notifies the appropriate model listeners.
	 *
	 * @param primaryKey the primary key of the service req
	 * @return the service req that was removed
	 * @throws NoSuchserviceReqException if a service req with the primary key could not be found
	 */
	@Override
	public serviceReq remove(Serializable primaryKey)
		throws NoSuchserviceReqException {
		Session session = null;

		try {
			session = openSession();

			serviceReq serviceReq = (serviceReq)session.get(serviceReqImpl.class,
					primaryKey);

			if (serviceReq == null) {
				if (_log.isDebugEnabled()) {
					_log.debug(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY + primaryKey);
				}

				throw new NoSuchserviceReqException(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY +
					primaryKey);
			}

			return remove(serviceReq);
		}
		catch (NoSuchserviceReqException nsee) {
			throw nsee;
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}
	}

	@Override
	protected serviceReq removeImpl(serviceReq serviceReq) {
		serviceReq = toUnwrappedModel(serviceReq);

		Session session = null;

		try {
			session = openSession();

			if (!session.contains(serviceReq)) {
				serviceReq = (serviceReq)session.get(serviceReqImpl.class,
						serviceReq.getPrimaryKeyObj());
			}

			if (serviceReq != null) {
				session.delete(serviceReq);
			}
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}

		if (serviceReq != null) {
			clearCache(serviceReq);
		}

		return serviceReq;
	}

	@Override
	public serviceReq updateImpl(serviceReq serviceReq) {
		serviceReq = toUnwrappedModel(serviceReq);

		boolean isNew = serviceReq.isNew();

		Session session = null;

		try {
			session = openSession();

			if (serviceReq.isNew()) {
				session.save(serviceReq);

				serviceReq.setNew(false);
			}
			else {
				serviceReq = (serviceReq)session.merge(serviceReq);
			}
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}

		finderCache.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);

		if (isNew) {
			finderCache.removeResult(FINDER_PATH_COUNT_ALL, FINDER_ARGS_EMPTY);
			finderCache.removeResult(FINDER_PATH_WITHOUT_PAGINATION_FIND_ALL,
				FINDER_ARGS_EMPTY);
		}

		entityCache.putResult(serviceReqModelImpl.ENTITY_CACHE_ENABLED,
			serviceReqImpl.class, serviceReq.getPrimaryKey(), serviceReq, false);

		serviceReq.resetOriginalValues();

		return serviceReq;
	}

	protected serviceReq toUnwrappedModel(serviceReq serviceReq) {
		if (serviceReq instanceof serviceReqImpl) {
			return serviceReq;
		}

		serviceReqImpl serviceReqImpl = new serviceReqImpl();

		serviceReqImpl.setNew(serviceReq.isNew());
		serviceReqImpl.setPrimaryKey(serviceReq.getPrimaryKey());

		serviceReqImpl.setServeReqID(serviceReq.getServeReqID());
		serviceReqImpl.setIssueCategory(serviceReq.getIssueCategory());
		serviceReqImpl.setIssueTitle(serviceReq.getIssueTitle());
		serviceReqImpl.setIssueDesc(serviceReq.getIssueDesc());
		serviceReqImpl.setIssueImpact(serviceReq.getIssueImpact());
		serviceReqImpl.setIssueUrgency(serviceReq.getIssueUrgency());

		return serviceReqImpl;
	}

	/**
	 * Returns the service req with the primary key or throws a {@link com.liferay.portal.kernel.exception.NoSuchModelException} if it could not be found.
	 *
	 * @param primaryKey the primary key of the service req
	 * @return the service req
	 * @throws NoSuchserviceReqException if a service req with the primary key could not be found
	 */
	@Override
	public serviceReq findByPrimaryKey(Serializable primaryKey)
		throws NoSuchserviceReqException {
		serviceReq serviceReq = fetchByPrimaryKey(primaryKey);

		if (serviceReq == null) {
			if (_log.isDebugEnabled()) {
				_log.debug(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY + primaryKey);
			}

			throw new NoSuchserviceReqException(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY +
				primaryKey);
		}

		return serviceReq;
	}

	/**
	 * Returns the service req with the primary key or throws a {@link NoSuchserviceReqException} if it could not be found.
	 *
	 * @param serveReqID the primary key of the service req
	 * @return the service req
	 * @throws NoSuchserviceReqException if a service req with the primary key could not be found
	 */
	@Override
	public serviceReq findByPrimaryKey(long serveReqID)
		throws NoSuchserviceReqException {
		return findByPrimaryKey((Serializable)serveReqID);
	}

	/**
	 * Returns the service req with the primary key or returns <code>null</code> if it could not be found.
	 *
	 * @param primaryKey the primary key of the service req
	 * @return the service req, or <code>null</code> if a service req with the primary key could not be found
	 */
	@Override
	public serviceReq fetchByPrimaryKey(Serializable primaryKey) {
		Serializable serializable = entityCache.getResult(serviceReqModelImpl.ENTITY_CACHE_ENABLED,
				serviceReqImpl.class, primaryKey);

		if (serializable == nullModel) {
			return null;
		}

		serviceReq serviceReq = (serviceReq)serializable;

		if (serviceReq == null) {
			Session session = null;

			try {
				session = openSession();

				serviceReq = (serviceReq)session.get(serviceReqImpl.class,
						primaryKey);

				if (serviceReq != null) {
					cacheResult(serviceReq);
				}
				else {
					entityCache.putResult(serviceReqModelImpl.ENTITY_CACHE_ENABLED,
						serviceReqImpl.class, primaryKey, nullModel);
				}
			}
			catch (Exception e) {
				entityCache.removeResult(serviceReqModelImpl.ENTITY_CACHE_ENABLED,
					serviceReqImpl.class, primaryKey);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return serviceReq;
	}

	/**
	 * Returns the service req with the primary key or returns <code>null</code> if it could not be found.
	 *
	 * @param serveReqID the primary key of the service req
	 * @return the service req, or <code>null</code> if a service req with the primary key could not be found
	 */
	@Override
	public serviceReq fetchByPrimaryKey(long serveReqID) {
		return fetchByPrimaryKey((Serializable)serveReqID);
	}

	@Override
	public Map<Serializable, serviceReq> fetchByPrimaryKeys(
		Set<Serializable> primaryKeys) {
		if (primaryKeys.isEmpty()) {
			return Collections.emptyMap();
		}

		Map<Serializable, serviceReq> map = new HashMap<Serializable, serviceReq>();

		if (primaryKeys.size() == 1) {
			Iterator<Serializable> iterator = primaryKeys.iterator();

			Serializable primaryKey = iterator.next();

			serviceReq serviceReq = fetchByPrimaryKey(primaryKey);

			if (serviceReq != null) {
				map.put(primaryKey, serviceReq);
			}

			return map;
		}

		Set<Serializable> uncachedPrimaryKeys = null;

		for (Serializable primaryKey : primaryKeys) {
			Serializable serializable = entityCache.getResult(serviceReqModelImpl.ENTITY_CACHE_ENABLED,
					serviceReqImpl.class, primaryKey);

			if (serializable != nullModel) {
				if (serializable == null) {
					if (uncachedPrimaryKeys == null) {
						uncachedPrimaryKeys = new HashSet<Serializable>();
					}

					uncachedPrimaryKeys.add(primaryKey);
				}
				else {
					map.put(primaryKey, (serviceReq)serializable);
				}
			}
		}

		if (uncachedPrimaryKeys == null) {
			return map;
		}

		StringBundler query = new StringBundler((uncachedPrimaryKeys.size() * 2) +
				1);

		query.append(_SQL_SELECT_SERVICEREQ_WHERE_PKS_IN);

		for (Serializable primaryKey : uncachedPrimaryKeys) {
			query.append((long)primaryKey);

			query.append(StringPool.COMMA);
		}

		query.setIndex(query.index() - 1);

		query.append(StringPool.CLOSE_PARENTHESIS);

		String sql = query.toString();

		Session session = null;

		try {
			session = openSession();

			Query q = session.createQuery(sql);

			for (serviceReq serviceReq : (List<serviceReq>)q.list()) {
				map.put(serviceReq.getPrimaryKeyObj(), serviceReq);

				cacheResult(serviceReq);

				uncachedPrimaryKeys.remove(serviceReq.getPrimaryKeyObj());
			}

			for (Serializable primaryKey : uncachedPrimaryKeys) {
				entityCache.putResult(serviceReqModelImpl.ENTITY_CACHE_ENABLED,
					serviceReqImpl.class, primaryKey, nullModel);
			}
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}

		return map;
	}

	/**
	 * Returns all the service reqs.
	 *
	 * @return the service reqs
	 */
	@Override
	public List<serviceReq> findAll() {
		return findAll(QueryUtil.ALL_POS, QueryUtil.ALL_POS, null);
	}

	/**
	 * Returns a range of all the service reqs.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link serviceReqModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param start the lower bound of the range of service reqs
	 * @param end the upper bound of the range of service reqs (not inclusive)
	 * @return the range of service reqs
	 */
	@Override
	public List<serviceReq> findAll(int start, int end) {
		return findAll(start, end, null);
	}

	/**
	 * Returns an ordered range of all the service reqs.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link serviceReqModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param start the lower bound of the range of service reqs
	 * @param end the upper bound of the range of service reqs (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of service reqs
	 */
	@Override
	public List<serviceReq> findAll(int start, int end,
		OrderByComparator<serviceReq> orderByComparator) {
		return findAll(start, end, orderByComparator, true);
	}

	/**
	 * Returns an ordered range of all the service reqs.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link serviceReqModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param start the lower bound of the range of service reqs
	 * @param end the upper bound of the range of service reqs (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @param retrieveFromCache whether to retrieve from the finder cache
	 * @return the ordered range of service reqs
	 */
	@Override
	public List<serviceReq> findAll(int start, int end,
		OrderByComparator<serviceReq> orderByComparator,
		boolean retrieveFromCache) {
		boolean pagination = true;
		FinderPath finderPath = null;
		Object[] finderArgs = null;

		if ((start == QueryUtil.ALL_POS) && (end == QueryUtil.ALL_POS) &&
				(orderByComparator == null)) {
			pagination = false;
			finderPath = FINDER_PATH_WITHOUT_PAGINATION_FIND_ALL;
			finderArgs = FINDER_ARGS_EMPTY;
		}
		else {
			finderPath = FINDER_PATH_WITH_PAGINATION_FIND_ALL;
			finderArgs = new Object[] { start, end, orderByComparator };
		}

		List<serviceReq> list = null;

		if (retrieveFromCache) {
			list = (List<serviceReq>)finderCache.getResult(finderPath,
					finderArgs, this);
		}

		if (list == null) {
			StringBundler query = null;
			String sql = null;

			if (orderByComparator != null) {
				query = new StringBundler(2 +
						(orderByComparator.getOrderByFields().length * 2));

				query.append(_SQL_SELECT_SERVICEREQ);

				appendOrderByComparator(query, _ORDER_BY_ENTITY_ALIAS,
					orderByComparator);

				sql = query.toString();
			}
			else {
				sql = _SQL_SELECT_SERVICEREQ;

				if (pagination) {
					sql = sql.concat(serviceReqModelImpl.ORDER_BY_JPQL);
				}
			}

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				if (!pagination) {
					list = (List<serviceReq>)QueryUtil.list(q, getDialect(),
							start, end, false);

					Collections.sort(list);

					list = Collections.unmodifiableList(list);
				}
				else {
					list = (List<serviceReq>)QueryUtil.list(q, getDialect(),
							start, end);
				}

				cacheResult(list);

				finderCache.putResult(finderPath, finderArgs, list);
			}
			catch (Exception e) {
				finderCache.removeResult(finderPath, finderArgs);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return list;
	}

	/**
	 * Removes all the service reqs from the database.
	 *
	 */
	@Override
	public void removeAll() {
		for (serviceReq serviceReq : findAll()) {
			remove(serviceReq);
		}
	}

	/**
	 * Returns the number of service reqs.
	 *
	 * @return the number of service reqs
	 */
	@Override
	public int countAll() {
		Long count = (Long)finderCache.getResult(FINDER_PATH_COUNT_ALL,
				FINDER_ARGS_EMPTY, this);

		if (count == null) {
			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(_SQL_COUNT_SERVICEREQ);

				count = (Long)q.uniqueResult();

				finderCache.putResult(FINDER_PATH_COUNT_ALL, FINDER_ARGS_EMPTY,
					count);
			}
			catch (Exception e) {
				finderCache.removeResult(FINDER_PATH_COUNT_ALL,
					FINDER_ARGS_EMPTY);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return count.intValue();
	}

	@Override
	protected Map<String, Integer> getTableColumnsMap() {
		return serviceReqModelImpl.TABLE_COLUMNS_MAP;
	}

	/**
	 * Initializes the service req persistence.
	 */
	public void afterPropertiesSet() {
	}

	public void destroy() {
		entityCache.removeCache(serviceReqImpl.class.getName());
		finderCache.removeCache(FINDER_CLASS_NAME_ENTITY);
		finderCache.removeCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
		finderCache.removeCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);
	}

	@ServiceReference(type = EntityCache.class)
	protected EntityCache entityCache;
	@ServiceReference(type = FinderCache.class)
	protected FinderCache finderCache;
	private static final String _SQL_SELECT_SERVICEREQ = "SELECT serviceReq FROM serviceReq serviceReq";
	private static final String _SQL_SELECT_SERVICEREQ_WHERE_PKS_IN = "SELECT serviceReq FROM serviceReq serviceReq WHERE serveReqID IN (";
	private static final String _SQL_COUNT_SERVICEREQ = "SELECT COUNT(serviceReq) FROM serviceReq serviceReq";
	private static final String _ORDER_BY_ENTITY_ALIAS = "serviceReq.";
	private static final String _NO_SUCH_ENTITY_WITH_PRIMARY_KEY = "No serviceReq exists with the primary key ";
	private static final Log _log = LogFactoryUtil.getLog(serviceReqPersistenceImpl.class);
}