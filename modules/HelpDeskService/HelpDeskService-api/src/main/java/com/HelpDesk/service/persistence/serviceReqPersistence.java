/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package com.HelpDesk.service.persistence;

import aQute.bnd.annotation.ProviderType;

import com.HelpDesk.exception.NoSuchserviceReqException;

import com.HelpDesk.model.serviceReq;

import com.liferay.portal.kernel.service.persistence.BasePersistence;

/**
 * The persistence interface for the service req service.
 *
 * <p>
 * Caching information and settings can be found in <code>portal.properties</code>
 * </p>
 *
 * @author Brian Wing Shun Chan
 * @see com.HelpDesk.service.persistence.impl.serviceReqPersistenceImpl
 * @see serviceReqUtil
 * @generated
 */
@ProviderType
public interface serviceReqPersistence extends BasePersistence<serviceReq> {
	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never modify or reference this interface directly. Always use {@link serviceReqUtil} to access the service req persistence. Modify <code>service.xml</code> and rerun ServiceBuilder to regenerate this interface.
	 */

	/**
	* Caches the service req in the entity cache if it is enabled.
	*
	* @param serviceReq the service req
	*/
	public void cacheResult(serviceReq serviceReq);

	/**
	* Caches the service reqs in the entity cache if it is enabled.
	*
	* @param serviceReqs the service reqs
	*/
	public void cacheResult(java.util.List<serviceReq> serviceReqs);

	/**
	* Creates a new service req with the primary key. Does not add the service req to the database.
	*
	* @param serveReqID the primary key for the new service req
	* @return the new service req
	*/
	public serviceReq create(long serveReqID);

	/**
	* Removes the service req with the primary key from the database. Also notifies the appropriate model listeners.
	*
	* @param serveReqID the primary key of the service req
	* @return the service req that was removed
	* @throws NoSuchserviceReqException if a service req with the primary key could not be found
	*/
	public serviceReq remove(long serveReqID) throws NoSuchserviceReqException;

	public serviceReq updateImpl(serviceReq serviceReq);

	/**
	* Returns the service req with the primary key or throws a {@link NoSuchserviceReqException} if it could not be found.
	*
	* @param serveReqID the primary key of the service req
	* @return the service req
	* @throws NoSuchserviceReqException if a service req with the primary key could not be found
	*/
	public serviceReq findByPrimaryKey(long serveReqID)
		throws NoSuchserviceReqException;

	/**
	* Returns the service req with the primary key or returns <code>null</code> if it could not be found.
	*
	* @param serveReqID the primary key of the service req
	* @return the service req, or <code>null</code> if a service req with the primary key could not be found
	*/
	public serviceReq fetchByPrimaryKey(long serveReqID);

	@Override
	public java.util.Map<java.io.Serializable, serviceReq> fetchByPrimaryKeys(
		java.util.Set<java.io.Serializable> primaryKeys);

	/**
	* Returns all the service reqs.
	*
	* @return the service reqs
	*/
	public java.util.List<serviceReq> findAll();

	/**
	* Returns a range of all the service reqs.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link serviceReqModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param start the lower bound of the range of service reqs
	* @param end the upper bound of the range of service reqs (not inclusive)
	* @return the range of service reqs
	*/
	public java.util.List<serviceReq> findAll(int start, int end);

	/**
	* Returns an ordered range of all the service reqs.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link serviceReqModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param start the lower bound of the range of service reqs
	* @param end the upper bound of the range of service reqs (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of service reqs
	*/
	public java.util.List<serviceReq> findAll(int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator<serviceReq> orderByComparator);

	/**
	* Returns an ordered range of all the service reqs.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link serviceReqModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param start the lower bound of the range of service reqs
	* @param end the upper bound of the range of service reqs (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @param retrieveFromCache whether to retrieve from the finder cache
	* @return the ordered range of service reqs
	*/
	public java.util.List<serviceReq> findAll(int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator<serviceReq> orderByComparator,
		boolean retrieveFromCache);

	/**
	* Removes all the service reqs from the database.
	*/
	public void removeAll();

	/**
	* Returns the number of service reqs.
	*
	* @return the number of service reqs
	*/
	public int countAll();
}