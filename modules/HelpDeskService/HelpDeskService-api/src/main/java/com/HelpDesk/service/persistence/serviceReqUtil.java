/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package com.HelpDesk.service.persistence;

import aQute.bnd.annotation.ProviderType;

import com.HelpDesk.model.serviceReq;

import com.liferay.osgi.util.ServiceTrackerFactory;

import com.liferay.portal.kernel.dao.orm.DynamicQuery;
import com.liferay.portal.kernel.service.ServiceContext;
import com.liferay.portal.kernel.util.OrderByComparator;

import org.osgi.util.tracker.ServiceTracker;

import java.util.List;

/**
 * The persistence utility for the service req service. This utility wraps {@link com.HelpDesk.service.persistence.impl.serviceReqPersistenceImpl} and provides direct access to the database for CRUD operations. This utility should only be used by the service layer, as it must operate within a transaction. Never access this utility in a JSP, controller, model, or other front-end class.
 *
 * <p>
 * Caching information and settings can be found in <code>portal.properties</code>
 * </p>
 *
 * @author Brian Wing Shun Chan
 * @see serviceReqPersistence
 * @see com.HelpDesk.service.persistence.impl.serviceReqPersistenceImpl
 * @generated
 */
@ProviderType
public class serviceReqUtil {
	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never modify this class directly. Modify <code>service.xml</code> and rerun ServiceBuilder to regenerate this class.
	 */

	/**
	 * @see com.liferay.portal.kernel.service.persistence.BasePersistence#clearCache()
	 */
	public static void clearCache() {
		getPersistence().clearCache();
	}

	/**
	 * @see com.liferay.portal.kernel.service.persistence.BasePersistence#clearCache(com.liferay.portal.kernel.model.BaseModel)
	 */
	public static void clearCache(serviceReq serviceReq) {
		getPersistence().clearCache(serviceReq);
	}

	/**
	 * @see com.liferay.portal.kernel.service.persistence.BasePersistence#countWithDynamicQuery(DynamicQuery)
	 */
	public static long countWithDynamicQuery(DynamicQuery dynamicQuery) {
		return getPersistence().countWithDynamicQuery(dynamicQuery);
	}

	/**
	 * @see com.liferay.portal.kernel.service.persistence.BasePersistence#findWithDynamicQuery(DynamicQuery)
	 */
	public static List<serviceReq> findWithDynamicQuery(
		DynamicQuery dynamicQuery) {
		return getPersistence().findWithDynamicQuery(dynamicQuery);
	}

	/**
	 * @see com.liferay.portal.kernel.service.persistence.BasePersistence#findWithDynamicQuery(DynamicQuery, int, int)
	 */
	public static List<serviceReq> findWithDynamicQuery(
		DynamicQuery dynamicQuery, int start, int end) {
		return getPersistence().findWithDynamicQuery(dynamicQuery, start, end);
	}

	/**
	 * @see com.liferay.portal.kernel.service.persistence.BasePersistence#findWithDynamicQuery(DynamicQuery, int, int, OrderByComparator)
	 */
	public static List<serviceReq> findWithDynamicQuery(
		DynamicQuery dynamicQuery, int start, int end,
		OrderByComparator<serviceReq> orderByComparator) {
		return getPersistence()
				   .findWithDynamicQuery(dynamicQuery, start, end,
			orderByComparator);
	}

	/**
	 * @see com.liferay.portal.kernel.service.persistence.BasePersistence#update(com.liferay.portal.kernel.model.BaseModel)
	 */
	public static serviceReq update(serviceReq serviceReq) {
		return getPersistence().update(serviceReq);
	}

	/**
	 * @see com.liferay.portal.kernel.service.persistence.BasePersistence#update(com.liferay.portal.kernel.model.BaseModel, ServiceContext)
	 */
	public static serviceReq update(serviceReq serviceReq,
		ServiceContext serviceContext) {
		return getPersistence().update(serviceReq, serviceContext);
	}

	/**
	* Caches the service req in the entity cache if it is enabled.
	*
	* @param serviceReq the service req
	*/
	public static void cacheResult(serviceReq serviceReq) {
		getPersistence().cacheResult(serviceReq);
	}

	/**
	* Caches the service reqs in the entity cache if it is enabled.
	*
	* @param serviceReqs the service reqs
	*/
	public static void cacheResult(List<serviceReq> serviceReqs) {
		getPersistence().cacheResult(serviceReqs);
	}

	/**
	* Creates a new service req with the primary key. Does not add the service req to the database.
	*
	* @param serveReqID the primary key for the new service req
	* @return the new service req
	*/
	public static serviceReq create(long serveReqID) {
		return getPersistence().create(serveReqID);
	}

	/**
	* Removes the service req with the primary key from the database. Also notifies the appropriate model listeners.
	*
	* @param serveReqID the primary key of the service req
	* @return the service req that was removed
	* @throws NoSuchserviceReqException if a service req with the primary key could not be found
	*/
	public static serviceReq remove(long serveReqID)
		throws com.HelpDesk.exception.NoSuchserviceReqException {
		return getPersistence().remove(serveReqID);
	}

	public static serviceReq updateImpl(serviceReq serviceReq) {
		return getPersistence().updateImpl(serviceReq);
	}

	/**
	* Returns the service req with the primary key or throws a {@link NoSuchserviceReqException} if it could not be found.
	*
	* @param serveReqID the primary key of the service req
	* @return the service req
	* @throws NoSuchserviceReqException if a service req with the primary key could not be found
	*/
	public static serviceReq findByPrimaryKey(long serveReqID)
		throws com.HelpDesk.exception.NoSuchserviceReqException {
		return getPersistence().findByPrimaryKey(serveReqID);
	}

	/**
	* Returns the service req with the primary key or returns <code>null</code> if it could not be found.
	*
	* @param serveReqID the primary key of the service req
	* @return the service req, or <code>null</code> if a service req with the primary key could not be found
	*/
	public static serviceReq fetchByPrimaryKey(long serveReqID) {
		return getPersistence().fetchByPrimaryKey(serveReqID);
	}

	public static java.util.Map<java.io.Serializable, serviceReq> fetchByPrimaryKeys(
		java.util.Set<java.io.Serializable> primaryKeys) {
		return getPersistence().fetchByPrimaryKeys(primaryKeys);
	}

	/**
	* Returns all the service reqs.
	*
	* @return the service reqs
	*/
	public static List<serviceReq> findAll() {
		return getPersistence().findAll();
	}

	/**
	* Returns a range of all the service reqs.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link serviceReqModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param start the lower bound of the range of service reqs
	* @param end the upper bound of the range of service reqs (not inclusive)
	* @return the range of service reqs
	*/
	public static List<serviceReq> findAll(int start, int end) {
		return getPersistence().findAll(start, end);
	}

	/**
	* Returns an ordered range of all the service reqs.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link serviceReqModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param start the lower bound of the range of service reqs
	* @param end the upper bound of the range of service reqs (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of service reqs
	*/
	public static List<serviceReq> findAll(int start, int end,
		OrderByComparator<serviceReq> orderByComparator) {
		return getPersistence().findAll(start, end, orderByComparator);
	}

	/**
	* Returns an ordered range of all the service reqs.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link serviceReqModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param start the lower bound of the range of service reqs
	* @param end the upper bound of the range of service reqs (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @param retrieveFromCache whether to retrieve from the finder cache
	* @return the ordered range of service reqs
	*/
	public static List<serviceReq> findAll(int start, int end,
		OrderByComparator<serviceReq> orderByComparator,
		boolean retrieveFromCache) {
		return getPersistence()
				   .findAll(start, end, orderByComparator, retrieveFromCache);
	}

	/**
	* Removes all the service reqs from the database.
	*/
	public static void removeAll() {
		getPersistence().removeAll();
	}

	/**
	* Returns the number of service reqs.
	*
	* @return the number of service reqs
	*/
	public static int countAll() {
		return getPersistence().countAll();
	}

	public static serviceReqPersistence getPersistence() {
		return _serviceTracker.getService();
	}

	private static ServiceTracker<serviceReqPersistence, serviceReqPersistence> _serviceTracker =
		ServiceTrackerFactory.open(serviceReqPersistence.class);
}