/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package com.HelpDesk.service;

import aQute.bnd.annotation.ProviderType;

import com.liferay.portal.kernel.service.ServiceWrapper;

/**
 * Provides a wrapper for {@link serviceReqLocalService}.
 *
 * @author Brian Wing Shun Chan
 * @see serviceReqLocalService
 * @generated
 */
@ProviderType
public class serviceReqLocalServiceWrapper implements serviceReqLocalService,
	ServiceWrapper<serviceReqLocalService> {
	public serviceReqLocalServiceWrapper(
		serviceReqLocalService serviceReqLocalService) {
		_serviceReqLocalService = serviceReqLocalService;
	}

	/**
	* Adds the service req to the database. Also notifies the appropriate model listeners.
	*
	* @param serviceReq the service req
	* @return the service req that was added
	*/
	@Override
	public com.HelpDesk.model.serviceReq addserviceReq(
		com.HelpDesk.model.serviceReq serviceReq) {
		return _serviceReqLocalService.addserviceReq(serviceReq);
	}

	/**
	* Creates a new service req with the primary key. Does not add the service req to the database.
	*
	* @param serveReqID the primary key for the new service req
	* @return the new service req
	*/
	@Override
	public com.HelpDesk.model.serviceReq createserviceReq(long serveReqID) {
		return _serviceReqLocalService.createserviceReq(serveReqID);
	}

	/**
	* Deletes the service req from the database. Also notifies the appropriate model listeners.
	*
	* @param serviceReq the service req
	* @return the service req that was removed
	*/
	@Override
	public com.HelpDesk.model.serviceReq deleteserviceReq(
		com.HelpDesk.model.serviceReq serviceReq) {
		return _serviceReqLocalService.deleteserviceReq(serviceReq);
	}

	/**
	* Deletes the service req with the primary key from the database. Also notifies the appropriate model listeners.
	*
	* @param serveReqID the primary key of the service req
	* @return the service req that was removed
	* @throws PortalException if a service req with the primary key could not be found
	*/
	@Override
	public com.HelpDesk.model.serviceReq deleteserviceReq(long serveReqID)
		throws com.liferay.portal.kernel.exception.PortalException {
		return _serviceReqLocalService.deleteserviceReq(serveReqID);
	}

	@Override
	public com.HelpDesk.model.serviceReq fetchserviceReq(long serveReqID) {
		return _serviceReqLocalService.fetchserviceReq(serveReqID);
	}

	/**
	* Returns the service req with the primary key.
	*
	* @param serveReqID the primary key of the service req
	* @return the service req
	* @throws PortalException if a service req with the primary key could not be found
	*/
	@Override
	public com.HelpDesk.model.serviceReq getserviceReq(long serveReqID)
		throws com.liferay.portal.kernel.exception.PortalException {
		return _serviceReqLocalService.getserviceReq(serveReqID);
	}

	/**
	* Updates the service req in the database or adds it if it does not yet exist. Also notifies the appropriate model listeners.
	*
	* @param serviceReq the service req
	* @return the service req that was updated
	*/
	@Override
	public com.HelpDesk.model.serviceReq updateserviceReq(
		com.HelpDesk.model.serviceReq serviceReq) {
		return _serviceReqLocalService.updateserviceReq(serviceReq);
	}

	@Override
	public com.liferay.portal.kernel.dao.orm.ActionableDynamicQuery getActionableDynamicQuery() {
		return _serviceReqLocalService.getActionableDynamicQuery();
	}

	@Override
	public com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery() {
		return _serviceReqLocalService.dynamicQuery();
	}

	@Override
	public com.liferay.portal.kernel.dao.orm.IndexableActionableDynamicQuery getIndexableActionableDynamicQuery() {
		return _serviceReqLocalService.getIndexableActionableDynamicQuery();
	}

	/**
	* @throws PortalException
	*/
	@Override
	public com.liferay.portal.kernel.model.PersistedModel deletePersistedModel(
		com.liferay.portal.kernel.model.PersistedModel persistedModel)
		throws com.liferay.portal.kernel.exception.PortalException {
		return _serviceReqLocalService.deletePersistedModel(persistedModel);
	}

	@Override
	public com.liferay.portal.kernel.model.PersistedModel getPersistedModel(
		java.io.Serializable primaryKeyObj)
		throws com.liferay.portal.kernel.exception.PortalException {
		return _serviceReqLocalService.getPersistedModel(primaryKeyObj);
	}

	/**
	* Returns the number of service reqs.
	*
	* @return the number of service reqs
	*/
	@Override
	public int getserviceReqsCount() {
		return _serviceReqLocalService.getserviceReqsCount();
	}

	/**
	* Returns the OSGi service identifier.
	*
	* @return the OSGi service identifier
	*/
	@Override
	public java.lang.String getOSGiServiceIdentifier() {
		return _serviceReqLocalService.getOSGiServiceIdentifier();
	}

	/**
	* Performs a dynamic query on the database and returns the matching rows.
	*
	* @param dynamicQuery the dynamic query
	* @return the matching rows
	*/
	@Override
	public <T> java.util.List<T> dynamicQuery(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery) {
		return _serviceReqLocalService.dynamicQuery(dynamicQuery);
	}

	/**
	* Performs a dynamic query on the database and returns a range of the matching rows.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link com.HelpDesk.model.impl.serviceReqModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param dynamicQuery the dynamic query
	* @param start the lower bound of the range of model instances
	* @param end the upper bound of the range of model instances (not inclusive)
	* @return the range of matching rows
	*/
	@Override
	public <T> java.util.List<T> dynamicQuery(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery, int start,
		int end) {
		return _serviceReqLocalService.dynamicQuery(dynamicQuery, start, end);
	}

	/**
	* Performs a dynamic query on the database and returns an ordered range of the matching rows.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link com.HelpDesk.model.impl.serviceReqModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param dynamicQuery the dynamic query
	* @param start the lower bound of the range of model instances
	* @param end the upper bound of the range of model instances (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of matching rows
	*/
	@Override
	public <T> java.util.List<T> dynamicQuery(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery, int start,
		int end,
		com.liferay.portal.kernel.util.OrderByComparator<T> orderByComparator) {
		return _serviceReqLocalService.dynamicQuery(dynamicQuery, start, end,
			orderByComparator);
	}

	/**
	* Returns a range of all the service reqs.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link com.HelpDesk.model.impl.serviceReqModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param start the lower bound of the range of service reqs
	* @param end the upper bound of the range of service reqs (not inclusive)
	* @return the range of service reqs
	*/
	@Override
	public java.util.List<com.HelpDesk.model.serviceReq> getserviceReqs(
		int start, int end) {
		return _serviceReqLocalService.getserviceReqs(start, end);
	}

	/**
	* Returns the number of rows matching the dynamic query.
	*
	* @param dynamicQuery the dynamic query
	* @return the number of rows matching the dynamic query
	*/
	@Override
	public long dynamicQueryCount(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery) {
		return _serviceReqLocalService.dynamicQueryCount(dynamicQuery);
	}

	/**
	* Returns the number of rows matching the dynamic query.
	*
	* @param dynamicQuery the dynamic query
	* @param projection the projection to apply to the query
	* @return the number of rows matching the dynamic query
	*/
	@Override
	public long dynamicQueryCount(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery,
		com.liferay.portal.kernel.dao.orm.Projection projection) {
		return _serviceReqLocalService.dynamicQueryCount(dynamicQuery,
			projection);
	}

	@Override
	public serviceReqLocalService getWrappedService() {
		return _serviceReqLocalService;
	}

	@Override
	public void setWrappedService(serviceReqLocalService serviceReqLocalService) {
		_serviceReqLocalService = serviceReqLocalService;
	}

	private serviceReqLocalService _serviceReqLocalService;
}