/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package com.HelpDesk.model;

import aQute.bnd.annotation.ProviderType;

import java.io.Serializable;

import java.util.ArrayList;
import java.util.List;

/**
 * This class is used by SOAP remote services.
 *
 * @author Brian Wing Shun Chan
 * @generated
 */
@ProviderType
public class serviceReqSoap implements Serializable {
	public static serviceReqSoap toSoapModel(serviceReq model) {
		serviceReqSoap soapModel = new serviceReqSoap();

		soapModel.setServeReqID(model.getServeReqID());
		soapModel.setIssueCategory(model.getIssueCategory());
		soapModel.setIssueTitle(model.getIssueTitle());
		soapModel.setIssueDesc(model.getIssueDesc());
		soapModel.setIssueImpact(model.getIssueImpact());
		soapModel.setIssueUrgency(model.getIssueUrgency());

		return soapModel;
	}

	public static serviceReqSoap[] toSoapModels(serviceReq[] models) {
		serviceReqSoap[] soapModels = new serviceReqSoap[models.length];

		for (int i = 0; i < models.length; i++) {
			soapModels[i] = toSoapModel(models[i]);
		}

		return soapModels;
	}

	public static serviceReqSoap[][] toSoapModels(serviceReq[][] models) {
		serviceReqSoap[][] soapModels = null;

		if (models.length > 0) {
			soapModels = new serviceReqSoap[models.length][models[0].length];
		}
		else {
			soapModels = new serviceReqSoap[0][0];
		}

		for (int i = 0; i < models.length; i++) {
			soapModels[i] = toSoapModels(models[i]);
		}

		return soapModels;
	}

	public static serviceReqSoap[] toSoapModels(List<serviceReq> models) {
		List<serviceReqSoap> soapModels = new ArrayList<serviceReqSoap>(models.size());

		for (serviceReq model : models) {
			soapModels.add(toSoapModel(model));
		}

		return soapModels.toArray(new serviceReqSoap[soapModels.size()]);
	}

	public serviceReqSoap() {
	}

	public long getPrimaryKey() {
		return _serveReqID;
	}

	public void setPrimaryKey(long pk) {
		setServeReqID(pk);
	}

	public long getServeReqID() {
		return _serveReqID;
	}

	public void setServeReqID(long serveReqID) {
		_serveReqID = serveReqID;
	}

	public String getIssueCategory() {
		return _issueCategory;
	}

	public void setIssueCategory(String issueCategory) {
		_issueCategory = issueCategory;
	}

	public String getIssueTitle() {
		return _issueTitle;
	}

	public void setIssueTitle(String issueTitle) {
		_issueTitle = issueTitle;
	}

	public String getIssueDesc() {
		return _issueDesc;
	}

	public void setIssueDesc(String issueDesc) {
		_issueDesc = issueDesc;
	}

	public String getIssueImpact() {
		return _issueImpact;
	}

	public void setIssueImpact(String issueImpact) {
		_issueImpact = issueImpact;
	}

	public String getIssueUrgency() {
		return _issueUrgency;
	}

	public void setIssueUrgency(String issueUrgency) {
		_issueUrgency = issueUrgency;
	}

	private long _serveReqID;
	private String _issueCategory;
	private String _issueTitle;
	private String _issueDesc;
	private String _issueImpact;
	private String _issueUrgency;
}