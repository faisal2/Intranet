/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package com.HelpDesk.model;

import aQute.bnd.annotation.ProviderType;

import com.liferay.expando.kernel.model.ExpandoBridge;

import com.liferay.portal.kernel.model.ModelWrapper;
import com.liferay.portal.kernel.service.ServiceContext;

import java.io.Serializable;

import java.util.HashMap;
import java.util.Map;
import java.util.Objects;

/**
 * <p>
 * This class is a wrapper for {@link serviceReq}.
 * </p>
 *
 * @author Brian Wing Shun Chan
 * @see serviceReq
 * @generated
 */
@ProviderType
public class serviceReqWrapper implements serviceReq, ModelWrapper<serviceReq> {
	public serviceReqWrapper(serviceReq serviceReq) {
		_serviceReq = serviceReq;
	}

	@Override
	public Class<?> getModelClass() {
		return serviceReq.class;
	}

	@Override
	public String getModelClassName() {
		return serviceReq.class.getName();
	}

	@Override
	public Map<String, Object> getModelAttributes() {
		Map<String, Object> attributes = new HashMap<String, Object>();

		attributes.put("serveReqID", getServeReqID());
		attributes.put("issueCategory", getIssueCategory());
		attributes.put("issueTitle", getIssueTitle());
		attributes.put("issueDesc", getIssueDesc());
		attributes.put("issueImpact", getIssueImpact());
		attributes.put("issueUrgency", getIssueUrgency());

		return attributes;
	}

	@Override
	public void setModelAttributes(Map<String, Object> attributes) {
		Long serveReqID = (Long)attributes.get("serveReqID");

		if (serveReqID != null) {
			setServeReqID(serveReqID);
		}

		String issueCategory = (String)attributes.get("issueCategory");

		if (issueCategory != null) {
			setIssueCategory(issueCategory);
		}

		String issueTitle = (String)attributes.get("issueTitle");

		if (issueTitle != null) {
			setIssueTitle(issueTitle);
		}

		String issueDesc = (String)attributes.get("issueDesc");

		if (issueDesc != null) {
			setIssueDesc(issueDesc);
		}

		String issueImpact = (String)attributes.get("issueImpact");

		if (issueImpact != null) {
			setIssueImpact(issueImpact);
		}

		String issueUrgency = (String)attributes.get("issueUrgency");

		if (issueUrgency != null) {
			setIssueUrgency(issueUrgency);
		}
	}

	@Override
	public boolean isCachedModel() {
		return _serviceReq.isCachedModel();
	}

	@Override
	public boolean isEscapedModel() {
		return _serviceReq.isEscapedModel();
	}

	@Override
	public boolean isNew() {
		return _serviceReq.isNew();
	}

	@Override
	public com.HelpDesk.model.serviceReq toEscapedModel() {
		return new serviceReqWrapper(_serviceReq.toEscapedModel());
	}

	@Override
	public com.HelpDesk.model.serviceReq toUnescapedModel() {
		return new serviceReqWrapper(_serviceReq.toUnescapedModel());
	}

	@Override
	public ExpandoBridge getExpandoBridge() {
		return _serviceReq.getExpandoBridge();
	}

	@Override
	public com.liferay.portal.kernel.model.CacheModel<com.HelpDesk.model.serviceReq> toCacheModel() {
		return _serviceReq.toCacheModel();
	}

	@Override
	public int compareTo(com.HelpDesk.model.serviceReq serviceReq) {
		return _serviceReq.compareTo(serviceReq);
	}

	@Override
	public int hashCode() {
		return _serviceReq.hashCode();
	}

	@Override
	public Serializable getPrimaryKeyObj() {
		return _serviceReq.getPrimaryKeyObj();
	}

	@Override
	public java.lang.Object clone() {
		return new serviceReqWrapper((serviceReq)_serviceReq.clone());
	}

	/**
	* Returns the issue category of this service req.
	*
	* @return the issue category of this service req
	*/
	@Override
	public java.lang.String getIssueCategory() {
		return _serviceReq.getIssueCategory();
	}

	/**
	* Returns the issue desc of this service req.
	*
	* @return the issue desc of this service req
	*/
	@Override
	public java.lang.String getIssueDesc() {
		return _serviceReq.getIssueDesc();
	}

	/**
	* Returns the issue impact of this service req.
	*
	* @return the issue impact of this service req
	*/
	@Override
	public java.lang.String getIssueImpact() {
		return _serviceReq.getIssueImpact();
	}

	/**
	* Returns the issue title of this service req.
	*
	* @return the issue title of this service req
	*/
	@Override
	public java.lang.String getIssueTitle() {
		return _serviceReq.getIssueTitle();
	}

	/**
	* Returns the issue urgency of this service req.
	*
	* @return the issue urgency of this service req
	*/
	@Override
	public java.lang.String getIssueUrgency() {
		return _serviceReq.getIssueUrgency();
	}

	@Override
	public java.lang.String toString() {
		return _serviceReq.toString();
	}

	@Override
	public java.lang.String toXmlString() {
		return _serviceReq.toXmlString();
	}

	/**
	* Returns the primary key of this service req.
	*
	* @return the primary key of this service req
	*/
	@Override
	public long getPrimaryKey() {
		return _serviceReq.getPrimaryKey();
	}

	/**
	* Returns the serve req ID of this service req.
	*
	* @return the serve req ID of this service req
	*/
	@Override
	public long getServeReqID() {
		return _serviceReq.getServeReqID();
	}

	@Override
	public void persist() {
		_serviceReq.persist();
	}

	@Override
	public void setCachedModel(boolean cachedModel) {
		_serviceReq.setCachedModel(cachedModel);
	}

	@Override
	public void setExpandoBridgeAttributes(ExpandoBridge expandoBridge) {
		_serviceReq.setExpandoBridgeAttributes(expandoBridge);
	}

	@Override
	public void setExpandoBridgeAttributes(
		com.liferay.portal.kernel.model.BaseModel<?> baseModel) {
		_serviceReq.setExpandoBridgeAttributes(baseModel);
	}

	@Override
	public void setExpandoBridgeAttributes(ServiceContext serviceContext) {
		_serviceReq.setExpandoBridgeAttributes(serviceContext);
	}

	/**
	* Sets the issue category of this service req.
	*
	* @param issueCategory the issue category of this service req
	*/
	@Override
	public void setIssueCategory(java.lang.String issueCategory) {
		_serviceReq.setIssueCategory(issueCategory);
	}

	/**
	* Sets the issue desc of this service req.
	*
	* @param issueDesc the issue desc of this service req
	*/
	@Override
	public void setIssueDesc(java.lang.String issueDesc) {
		_serviceReq.setIssueDesc(issueDesc);
	}

	/**
	* Sets the issue impact of this service req.
	*
	* @param issueImpact the issue impact of this service req
	*/
	@Override
	public void setIssueImpact(java.lang.String issueImpact) {
		_serviceReq.setIssueImpact(issueImpact);
	}

	/**
	* Sets the issue title of this service req.
	*
	* @param issueTitle the issue title of this service req
	*/
	@Override
	public void setIssueTitle(java.lang.String issueTitle) {
		_serviceReq.setIssueTitle(issueTitle);
	}

	/**
	* Sets the issue urgency of this service req.
	*
	* @param issueUrgency the issue urgency of this service req
	*/
	@Override
	public void setIssueUrgency(java.lang.String issueUrgency) {
		_serviceReq.setIssueUrgency(issueUrgency);
	}

	@Override
	public void setNew(boolean n) {
		_serviceReq.setNew(n);
	}

	/**
	* Sets the primary key of this service req.
	*
	* @param primaryKey the primary key of this service req
	*/
	@Override
	public void setPrimaryKey(long primaryKey) {
		_serviceReq.setPrimaryKey(primaryKey);
	}

	@Override
	public void setPrimaryKeyObj(Serializable primaryKeyObj) {
		_serviceReq.setPrimaryKeyObj(primaryKeyObj);
	}

	/**
	* Sets the serve req ID of this service req.
	*
	* @param serveReqID the serve req ID of this service req
	*/
	@Override
	public void setServeReqID(long serveReqID) {
		_serviceReq.setServeReqID(serveReqID);
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}

		if (!(obj instanceof serviceReqWrapper)) {
			return false;
		}

		serviceReqWrapper serviceReqWrapper = (serviceReqWrapper)obj;

		if (Objects.equals(_serviceReq, serviceReqWrapper._serviceReq)) {
			return true;
		}

		return false;
	}

	@Override
	public serviceReq getWrappedModel() {
		return _serviceReq;
	}

	@Override
	public boolean isEntityCacheEnabled() {
		return _serviceReq.isEntityCacheEnabled();
	}

	@Override
	public boolean isFinderCacheEnabled() {
		return _serviceReq.isFinderCacheEnabled();
	}

	@Override
	public void resetOriginalValues() {
		_serviceReq.resetOriginalValues();
	}

	private final serviceReq _serviceReq;
}