<%@ include file="/META-INF/resources/init.jsp" %>
<%@page import="com.liferay.portal.kernel.util.ParamUtil"%>
<%@page import="java.util.Map"%>



<portlet:actionURL var="Summary" windowState="normal" name="Summary">
</portlet:actionURL>
 

<br/>

<div class="row" >
    <div class="col-sm-3" ><h4><u><b>Step2(of 3): Description</b></u></h4></div>
    
</div>
 

<h4><label class="lightGreyfont">Category Name:</label> <span class="label label-default"><%=renderRequest.getParameter("cat")%></span></h4><br/>

<form name="<portlet:namespace/>fm"
	action="<%=Summary.toString()%>" method="post"
	name="Summary">

<div class="container">
<div class="form-group">

       
       <aui:input name="Title"  required="true" resizable="true"/>
                  
      
    </div>
   </div>   
<div class="container">
<div class="form-group">
      
      
      <aui:input type="textarea"  name="Description"  required="true" resizable="true"/>
    </div>
</div>

<div class="container-fluid" style="margin-left:10px; margin-top:20px;">
<div class="row">
<div  class="col-sm-3" ><h5><i class="fa fa-bars"></i>Impact:</h5></div>
<div class="col-sm-3" >
<select class="form-control" style="border-width:1px" name="<portlet:namespace/>impact"
			id="<portlet:namespace/>impact">

			<option>0-Negligible/None</option>
			<option>1-Major Functionality Loss</option>
			<option>2-Minor Functionality Loss</option>
			<option>3-Total Loss of Service</option>
		
		</select>

</div>
<div class="col-sm-3"><h5>Urgency:</h5></div>
<div class="col-sm-3">
<select class="form-control" style="border-width:1px" name="<portlet:namespace/>urgency"
			id="<portlet:namespace/>urgency">

			<option>0-Low</option>
			<option>1-Average</option>
			<option>2-High</option>
			<option>3-Critical</option>
		
		</select>


</div>
</div>
</div>
<div class="container">
<br/>


<input type="submit" class="btn btn-primary" name="Summary" id="Summary"style="float: right"
			value="Next &raquo;" />

</form>
<portlet:renderURL var="viewGreetingURL">
    <portlet:param name="mvcPath" value="/view.jsp" />
</portlet:renderURL>

<p><a href="<%= viewGreetingURL %>" class="btn btn-primary">&laquo; Back</a></p>
</div>

    
