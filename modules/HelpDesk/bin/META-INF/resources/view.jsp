<%@page import="com.liferay.portal.kernel.util.ListUtil"%>
<%@page import="com.HelpDesk.service.serviceReqLocalServiceUtil"%>
<%@page import="com.HelpDesk.model.serviceReq"%>
<%@page import="java.util.List"%>
<%@ include file="/init.jsp" %>

<liferay-ui:tabs names="View Existing Request,Create New Request,KnowledgeBase" refresh="<%=false %>">
 <liferay-ui:section>
 <%
            	List<serviceReq> list= serviceReqLocalServiceUtil.getserviceReqs(0, serviceReqLocalServiceUtil.getserviceReqsCount());
        %>
        
<liferay-ui:search-container delta ="1000" >

					
  					
  					<liferay-ui:search-container-results  results="<%= ListUtil.subList(list, searchContainer.getStart(), searchContainer.getEnd()) %>" />
  		<liferay-ui:search-container-row className="com.HelpDesk.model.serviceReq"  modelVar="servicereq">
  		    <liferay-ui:search-container-column-text property="serveReqID"  />
        	<liferay-ui:search-container-column-text property="issueCategory"  />
        	<liferay-ui:search-container-column-text property="issueTitle"  />        
        	<liferay-ui:search-container-column-text property="issueDesc"  />
        	<liferay-ui:search-container-column-text property="issueImpact" />
        	<liferay-ui:search-container-column-text property="issueUrgency"  />
        
   		</liferay-ui:search-container-row>    
   		<liferay-ui:search-iterator />
</liferay-ui:search-container>
 
 </liferay-ui:section>
<liferay-ui:section>
<portlet:actionURL var="DescribeIssue" windowState="normal" name="DescribeIssue">

</portlet:actionURL>
<br/>
<div class="row" >
    <div class="col-sm-3" ><h4><u><b>Step1(of 3): Categorize Service</b></u></h4></div>
    
</div>


<form name="<portlet:namespace/>fm"
	action="<%=DescribeIssue.toString()%>" method="post"
	name="DescribeIssue">
	<!-- <div class="col-xs-2">
		<label for="exampleFormControlSelect1" style="float: right;">Select
			Category:</label>
	</div> -->
	<div class="col-xs-12" style="padding-left: 200px; width:500px;">


		<%-- <select class="form-control" style="border-width:1px" name="<portlet:namespace/>category"
			id="<portlet:namespace/>category">

			<option>Sales</option>
			<option>Service</option>
			<option>Functionality</option>
		</select> --%>
			<aui:select name="category:"  style="border-width:1px; text-align: center; font-family:arial;" required="true" >
    <aui:option value="Sales">Sales </aui:option>
    <aui:option value="Service">Service </aui:option>
    <aui:option value="Functionality">Functionality </aui:option>
</aui:select>
	</div>

	<div class="container">

		<input type="submit" class="btn btn-primary" name="describe" id="describe"style="float: right"
			value="Next &raquo;" />
	</div>
</form>
</liferay-ui:section>
<liferay-ui:section>
<p> <strong><a name "top">FREQUENTLY ASKED QUESTIONS</a></strong></p>
<p>&nbsp;</p>
     <script type="text/javascript" src="//code.jquery.com/jquery-1.11.0.min.js"></script>

<script>

$(document).ready(function() {

	$('.faq_question').click(function() {

		if ($(this).parent().is('.open')){
			$(this).closest('.faq').find('.faq_answer_container').animate({'height':'0'},500);
			$(this).closest('.faq').removeClass('open');

			}else{
				var newHeight =$(this).closest('.faq').find('.faq_answer').height() +'px';
				$(this).closest('.faq').find('.faq_answer_container').animate({'height':newHeight},500);
				$(this).closest('.faq').addClass('open');
			}

	});

});
</script>

<style>
/*FAQS*/
.faq_question {
    margin: 0px;
    padding: 0px 0px 5px 0px;
    display: inline-block;
    cursor: pointer;
    font-weight: bold;
}
 
.faq_answer_container {
    height: 0px;
    overflow: hidden;
    padding: 0px;
}
</style>

<div class="faq_container">
   <div class="faq">
      <div class="faq_question"><b>1.Question </b></div>
           <div class="faq_answer_container">
              <div class="faq_answer">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Lorem ipsum dolor sit amet, eu pede vehicula, ut dignissim. Eu urna ridiculus aliquet, nibh sodales libero non, in non lorem in, in nec tellus tincidunt vulputate nulla integer, ac dictum arcu ornare in vitae amet. Blandit amet odio hac dui, tortor vitae vitae, ipsum nullam nisl nullam nunc in, luctus quam, pede aut turpis et amet lobortis eu. Nunc dignissim, nec in vehicula luctus, magna nonummy rutrum tellus dictum magna consequat, et sed proin. Feugiat ac, ante tempora aliquam a convallis lacus duis, gravida in lorem ipsum eget, id nunc tempus porta eu ac, tincidunt hendrerit iaculis ultricies placerat non. Dui erat etiam sed velit senectus nec, potenti quam senectus, nulla a a, montes sit, tristique ac sed interdum quam. Ac ante et augue purus, nunc in eleifend vel vel, nunc urna id magna eget donec.</div>
           </div>        
    </div>
 </div>
 <div class="faq_container">
   <div class="faq">
      <div class="faq_question"><b>2.Question </b></div>
           <div class="faq_answer_container">
              <div class="faq_answer">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Lorem ipsum dolor sit amet, eu pede vehicula, ut dignissim. Eu urna ridiculus aliquet, nibh sodales libero non, in non lorem in, in nec tellus tincidunt vulputate nulla integer, ac dictum arcu ornare in vitae amet. Blandit amet odio hac dui, tortor vitae vitae, ipsum nullam nisl nullam nunc in, luctus quam, pede aut turpis et amet lobortis eu. Nunc dignissim, nec in vehicula luctus, magna nonummy rutrum tellus dictum magna consequat, et sed proin. Feugiat ac, ante tempora aliquam a convallis lacus duis, gravida in lorem ipsum eget, id nunc tempus porta eu ac, tincidunt hendrerit iaculis ultricies placerat non. Dui erat etiam sed velit senectus nec, potenti quam senectus, nulla a a, montes sit, tristique ac sed interdum quam. Ac ante et augue purus, nunc in eleifend vel vel, nunc urna id magna eget donec.</div>
           </div>        
    </div>
 </div>
  <div class="faq_container">
   <div class="faq">
      <div class="faq_question"><b>3.Question </b></div>
           <div class="faq_answer_container">
              <div class="faq_answer">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Lorem ipsum dolor sit amet, eu pede vehicula, ut dignissim. Eu urna ridiculus aliquet, nibh sodales libero non, in non lorem in, in nec tellus tincidunt vulputate nulla integer, ac dictum arcu ornare in vitae amet. Blandit amet odio hac dui, tortor vitae vitae, ipsum nullam nisl nullam nunc in, luctus quam, pede aut turpis et amet lobortis eu. Nunc dignissim, nec in vehicula luctus, magna nonummy rutrum tellus dictum magna consequat, et sed proin. Feugiat ac, ante tempora aliquam a convallis lacus duis, gravida in lorem ipsum eget, id nunc tempus porta eu ac, tincidunt hendrerit iaculis ultricies placerat non. Dui erat etiam sed velit senectus nec, potenti quam senectus, nulla a a, montes sit, tristique ac sed interdum quam. Ac ante et augue purus, nunc in eleifend vel vel, nunc urna id magna eget donec.</div>
           </div>        
    </div>
 </div>
  <div class="faq_container">
   <div class="faq">
      <div class="faq_question"><b>4.Question </b></div>
           <div class="faq_answer_container">
              <div class="faq_answer">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Lorem ipsum dolor sit amet, eu pede vehicula, ut dignissim. Eu urna ridiculus aliquet, nibh sodales libero non, in non lorem in, in nec tellus tincidunt vulputate nulla integer, ac dictum arcu ornare in vitae amet. Blandit amet odio hac dui, tortor vitae vitae, ipsum nullam nisl nullam nunc in, luctus quam, pede aut turpis et amet lobortis eu. Nunc dignissim, nec in vehicula luctus, magna nonummy rutrum tellus dictum magna consequat, et sed proin. Feugiat ac, ante tempora aliquam a convallis lacus duis, gravida in lorem ipsum eget, id nunc tempus porta eu ac, tincidunt hendrerit iaculis ultricies placerat non. Dui erat etiam sed velit senectus nec, potenti quam senectus, nulla a a, montes sit, tristique ac sed interdum quam. Ac ante et augue purus, nunc in eleifend vel vel, nunc urna id magna eget donec.</div>
           </div>        
    </div>
 </div>
  <div class="faq_container">
   <div class="faq">
      <div class="faq_question"><b>5.Question </b></div>
           <div class="faq_answer_container">
              <div class="faq_answer">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Lorem ipsum dolor sit amet, eu pede vehicula, ut dignissim. Eu urna ridiculus aliquet, nibh sodales libero non, in non lorem in, in nec tellus tincidunt vulputate nulla integer, ac dictum arcu ornare in vitae amet. Blandit amet odio hac dui, tortor vitae vitae, ipsum nullam nisl nullam nunc in, luctus quam, pede aut turpis et amet lobortis eu. Nunc dignissim, nec in vehicula luctus, magna nonummy rutrum tellus dictum magna consequat, et sed proin. Feugiat ac, ante tempora aliquam a convallis lacus duis, gravida in lorem ipsum eget, id nunc tempus porta eu ac, tincidunt hendrerit iaculis ultricies placerat non. Dui erat etiam sed velit senectus nec, potenti quam senectus, nulla a a, montes sit, tristique ac sed interdum quam. Ac ante et augue purus, nunc in eleifend vel vel, nunc urna id magna eget donec.</div>
           </div>        
    </div>
 </div>
  <div class="faq_container">
   <div class="faq">
      <div class="faq_question"><b>6.Question </b></div>
           <div class="faq_answer_container">
              <div class="faq_answer">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Lorem ipsum dolor sit amet, eu pede vehicula, ut dignissim. Eu urna ridiculus aliquet, nibh sodales libero non, in non lorem in, in nec tellus tincidunt vulputate nulla integer, ac dictum arcu ornare in vitae amet. Blandit amet odio hac dui, tortor vitae vitae, ipsum nullam nisl nullam nunc in, luctus quam, pede aut turpis et amet lobortis eu. Nunc dignissim, nec in vehicula luctus, magna nonummy rutrum tellus dictum magna consequat, et sed proin. Feugiat ac, ante tempora aliquam a convallis lacus duis, gravida in lorem ipsum eget, id nunc tempus porta eu ac, tincidunt hendrerit iaculis ultricies placerat non. Dui erat etiam sed velit senectus nec, potenti quam senectus, nulla a a, montes sit, tristique ac sed interdum quam. Ac ante et augue purus, nunc in eleifend vel vel, nunc urna id magna eget donec.</div>
           </div>        
    </div>
 </div>
  <div class="faq_container">
   <div class="faq">
      <div class="faq_question"><b>7.Question </b></div>
           <div class="faq_answer_container">
              <div class="faq_answer">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Lorem ipsum dolor sit amet, eu pede vehicula, ut dignissim. Eu urna ridiculus aliquet, nibh sodales libero non, in non lorem in, in nec tellus tincidunt vulputate nulla integer, ac dictum arcu ornare in vitae amet. Blandit amet odio hac dui, tortor vitae vitae, ipsum nullam nisl nullam nunc in, luctus quam, pede aut turpis et amet lobortis eu. Nunc dignissim, nec in vehicula luctus, magna nonummy rutrum tellus dictum magna consequat, et sed proin. Feugiat ac, ante tempora aliquam a convallis lacus duis, gravida in lorem ipsum eget, id nunc tempus porta eu ac, tincidunt hendrerit iaculis ultricies placerat non. Dui erat etiam sed velit senectus nec, potenti quam senectus, nulla a a, montes sit, tristique ac sed interdum quam. Ac ante et augue purus, nunc in eleifend vel vel, nunc urna id magna eget donec.</div>
           </div>        
    </div>
 </div>
  <div class="faq_container">
   <div class="faq">
      <div class="faq_question"><b>8.Question </b></div>
           <div class="faq_answer_container">
              <div class="faq_answer">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Lorem ipsum dolor sit amet, eu pede vehicula, ut dignissim. Eu urna ridiculus aliquet, nibh sodales libero non, in non lorem in, in nec tellus tincidunt vulputate nulla integer, ac dictum arcu ornare in vitae amet. Blandit amet odio hac dui, tortor vitae vitae, ipsum nullam nisl nullam nunc in, luctus quam, pede aut turpis et amet lobortis eu. Nunc dignissim, nec in vehicula luctus, magna nonummy rutrum tellus dictum magna consequat, et sed proin. Feugiat ac, ante tempora aliquam a convallis lacus duis, gravida in lorem ipsum eget, id nunc tempus porta eu ac, tincidunt hendrerit iaculis ultricies placerat non. Dui erat etiam sed velit senectus nec, potenti quam senectus, nulla a a, montes sit, tristique ac sed interdum quam. Ac ante et augue purus, nunc in eleifend vel vel, nunc urna id magna eget donec.</div>
           </div>        
    </div>
 </div>
  <div class="faq_container">
   <div class="faq">
      <div class="faq_question"><b>9.Question </b></div>
           <div class="faq_answer_container">
              <div class="faq_answer">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Lorem ipsum dolor sit amet, eu pede vehicula, ut dignissim. Eu urna ridiculus aliquet, nibh sodales libero non, in non lorem in, in nec tellus tincidunt vulputate nulla integer, ac dictum arcu ornare in vitae amet. Blandit amet odio hac dui, tortor vitae vitae, ipsum nullam nisl nullam nunc in, luctus quam, pede aut turpis et amet lobortis eu. Nunc dignissim, nec in vehicula luctus, magna nonummy rutrum tellus dictum magna consequat, et sed proin. Feugiat ac, ante tempora aliquam a convallis lacus duis, gravida in lorem ipsum eget, id nunc tempus porta eu ac, tincidunt hendrerit iaculis ultricies placerat non. Dui erat etiam sed velit senectus nec, potenti quam senectus, nulla a a, montes sit, tristique ac sed interdum quam. Ac ante et augue purus, nunc in eleifend vel vel, nunc urna id magna eget donec.</div>
           </div>        
    </div>
 </div>
 <a href="#top">Top</a>

    </liferay-ui:section>
</liferay-ui:tabs>