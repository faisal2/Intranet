<%@ include file="/META-INF/resources/init.jsp" %>



<portlet:actionURL var="ThankYou" windowState="normal" name="ThankYou">

</portlet:actionURL>

<portlet:renderURL var="describejsp">
<portlet:param name="mvcPath" value="/META-INF/resources/DescribeIssue.jsp"/>

</portlet:renderURL>


<form name="<portlet:namespace/>fm"
	action="<%=ThankYou.toString()%>" method="post"
	name="ThankYou">
<div class="row" >
    <div class="col-sm-3" ><h4><u><b>Step3(of 3): Summary</b></u></h4></div>
    
</div>
<div class="row-fluid"  style="background-color:silver;margin-top:30px; color:white;">
    <div class="span12" ><h4><b>Service Request Category</b></h4></div>
    
</div>



 <div class="row">

<div class="col-sm-2"><h4><label class="lightGreyfont" style="float:right;">Service Request Type:</label></h4></div>
<div class="col-sm-10"><h4><%=renderRequest.getParameter("Cat3")%></h4></div>
</div>

<div class="row-fluid"  style="background-color:silver; color:white;">
    <div class="span12" ><h4><b>Service Request Details</b></h4></div>
    
</div>



  <div class="row">

<div class="col-sm-2"><h4><label class="lightGreyfont" style="float:right;">Service request Title:</label></h4></div>
<div class="col-sm-10"><h4><%=renderRequest.getParameter("Title")%></h4></div>
</div>




  <div class="row">

<div class="col-sm-2"><h4><label class="lightGreyfont" style="float:right;">Service request Details:</label></h4></div>
<div class="col-sm-10"><h4><%=renderRequest.getParameter("Description")%></h4></div>
</div>


<div class="row-fluid"  style="background-color:silver; color:white;">
    <div class="span12" ><h4><b>Additional Details</b></h4></div>
    
</div>

<div class="row">
<div  class="col-sm-2"><h4><label class="lightGreyfont" style="float:right;">Impact:</label></h4></div>
<div class="col-sm-10"><h4><%=renderRequest.getParameter("impact")%></h4></div>

</div>



<div class="row">

<div class="col-sm-2"><h4><label class="lightGreyfont" style="float:right;">Urgency:</label></h4></div>
<div class="col-sm-10"><h4><%=renderRequest.getParameter("urgency")%></h4></div>
</div>




<div class="container-fluid" style="padding-left:10px; margin-top:15px;">
<div class="row">
<label  class="lightGreyfont" style="margin-top:20px;">Please upload relevant documents (if any)</label>
<input type="file" id="myFile" style="margin-top:20px;">
</div>
</div> 




<div class="container1" style="margin-top:20px;" >

<input type="submit" class="btn btn-primary" name="Summary" id="Summary"style="float: right"
			value="Submit Service Request &raquo;" />
</form>




<portlet:renderURL var="viewGreetingURL">
    <portlet:param name="mvcPath" value="/DescribeIssue.jsp" />
</portlet:renderURL>

<p><a href="<%= viewGreetingURL %>" class="btn btn-primary"> &laquo; Back</a></p>
</div>

