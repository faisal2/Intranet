package com.HelpDesk.portlet;

import com.HelpDesk.constants.HelpDeskPortletKeys;
import com.HelpDesk.model.serviceReq;
import com.HelpDesk.service.serviceReqLocalServiceUtil;
import com.liferay.portal.kernel.portlet.bridges.mvc.MVCPortlet;

import java.io.IOException;
import java.util.Random;

import javax.portlet.ActionRequest;
import javax.portlet.ActionResponse;
import javax.portlet.Portlet;
import javax.portlet.PortletException;
import javax.portlet.PortletSession;

import org.osgi.service.component.annotations.Component;

/**
 * @author user
 */
@Component(
	immediate = true,
	property = {
		"com.liferay.portlet.display-category=category.sample",
		"com.liferay.portlet.instanceable=true",
		"javax.portlet.display-name=HelpDesk Portlet",
		"javax.portlet.init-param.template-path=/",
		"javax.portlet.init-param.view-template=/view.jsp",
		"javax.portlet.name=" + HelpDeskPortletKeys.HelpDesk,
		"javax.portlet.resource-bundle=content.Language",
		"javax.portlet.security-role-ref=power-user,user"
	},
	service = Portlet.class
)
public class HelpDeskPortlet extends MVCPortlet {
	public void DescribeIssue(ActionRequest actionRequest,
            ActionResponse actionResponse) throws IOException, PortletException {
		
		String category = (String)actionRequest.getParameter("category:");
		
		
		/*System.out.println(category);*/
		
		PortletSession session = actionRequest.getPortletSession();
		session.setAttribute("cat2",category, PortletSession.PORTLET_SCOPE);
		/*String redirect=(String)actionRequest.getParameter("backURL");
		System.out.println(redirect);
		actionResponse.setRenderParameter("redirect",redirect);*/
	
		actionResponse.setRenderParameter("cat",category);
		
		actionResponse.setRenderParameter("mvcPath","/DescribeIssue.jsp");
		
	}
	public void Summary(ActionRequest arequest,
            ActionResponse aresponse) throws IOException, PortletException {
		
		
		
		String Title = (String)arequest.getParameter("Title");
		String Description = (String)arequest.getParameter("Description");
		String Impact = (String)arequest.getParameter("impact");
		String Urgency = (String)arequest.getParameter("urgency");
		/*String redirect1=(String)arequest.getParameter("backURL1");*/
		PortletSession session = arequest.getPortletSession();
		session.setAttribute("title",Title, PortletSession.PORTLET_SCOPE);
		session.setAttribute("description",Description, PortletSession.PORTLET_SCOPE);
		session.setAttribute("impact",Impact, PortletSession.PORTLET_SCOPE);
		session.setAttribute("urgency",Urgency, PortletSession.PORTLET_SCOPE);
		String portletScopeValue = (String) session.getAttribute("cat2", PortletSession.PORTLET_SCOPE);
		System.out.println(portletScopeValue);
		System.out.println(Title);
		System.out.println(Description);
		System.out.println(Impact);
		System.out.println(Urgency);
		
		
		/*aresponse.setRenderParameter("redirct1",redirect1);*/
		
		aresponse.setRenderParameter("impact",Impact);
		aresponse.setRenderParameter("urgency",Urgency);
		aresponse.setRenderParameter("Cat3",portletScopeValue);
		aresponse.setRenderParameter("Title",Title);
		aresponse.setRenderParameter("Description",Description);
		aresponse.setRenderParameter("mvcPath","/Summary.jsp");
	}
	public void ThankYou(ActionRequest actionRequest,
            ActionResponse actionResponse) throws IOException, PortletException {
		
		
	
		PortletSession session = actionRequest.getPortletSession();
		String Category = (String) session.getAttribute("cat2", PortletSession.PORTLET_SCOPE);
		String Title = (String) session.getAttribute("title", PortletSession.PORTLET_SCOPE);
		String Description = (String) session.getAttribute("description", PortletSession.PORTLET_SCOPE);
		String Impact = (String) session.getAttribute("impact", PortletSession.PORTLET_SCOPE);
		String Urgency = (String) session.getAttribute("urgency", PortletSession.PORTLET_SCOPE);
		
		
		System.out.println(Category+Title+Description+Impact+Urgency);
		Random rnd = new Random();
		int n = 100000 + rnd.nextInt(900000);
		String random= Integer.toString(n);
		System.out.println(random);
		try {
			serviceReq ServiceReq = serviceReqLocalServiceUtil.createserviceReq(n);
		
			ServiceReq.setIssueCategory(Category);
			ServiceReq.setIssueTitle(Title);
			ServiceReq.setIssueDesc(Description);
			ServiceReq.setIssueImpact(Impact);
			ServiceReq.setIssueUrgency(Urgency);
			serviceReqLocalServiceUtil.addserviceReq(ServiceReq);
			System.out.println("Successfully added======>>");
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		actionResponse.setRenderParameter("random",random);
		actionResponse.setRenderParameter("mvcPath","/ThankYou.jsp");
	}
	public void init() throws PortletException { 
		super.init(); 
		addProcessActionSuccessMessage = false;
		}
	}
