<!DOCTYPE html>

<#include init />

<html class="${root_css_class}" dir="<@liferay.language key="lang.dir" />" lang="${w3c_language_id}">

<head>
	<title>${the_title} - ${company_name}</title>

	<meta content="initial-scale=1.0, width=device-width" name="viewport" />

	<@liferay_util["include"] page=top_head_include />
</head>

<body class="${css_class}">

<@liferay_ui["quick-access"] contentId="#main-content" />

<@liferay_util["include"] page=body_top_include />

<@liferay.control_menu />

<div class="container-fluid">
	<header id="banner" role="banner">
	
		
	<div class="row" style="background-color:sandybrown;">
	<#if !is_signed_in>
	<a data-redirect="${is_login_redirect_required?string}" href="${sign_in_url}"  id="sign-in" rel="nofollow">${sign_in_text}</a>
		</#if>	
		<div class="col-md-2" style="margin-top: 45px";>
		<div id="heading">
		
			<h1 class="site-title">
				<a class="${logo_css_class}" href="${site_default_url}" title="<@liferay.language_format arguments="${site_name}" key="go-to-x" />">
					<img alt="${logo_description}" height="${site_logo_height}" src="${site_logo}" width="${site_logo_width}" />
				</a>

				<#if show_site_name>
					<span class="site-name" title="<@liferay.language_format arguments="${site_name}" key="go-to-x" />">
						${site_name}
					</span>
				</#if>
			</h1>
		</div>
		</div>
		
		<div class="col-md-10" style="margin-top: 45px";>
		<#if has_navigation && is_setup_complete>
			<#include "${full_templates_path}/navigation.ftl" />
		</#if>
		</div>
	</div>	
	</header>

	<section id="content">
		<h1 class="hide-accessible">${the_title}</h1>

		<!-- <nav id="breadcrumbs">
			<@liferay.breadcrumbs />
		</nav> -->

		<#if selectable>
			<@liferay_util["include"] page=content_include />
		<#else>
			${portletDisplay.recycle()}

			${portletDisplay.setTitle(the_title)}

			<@liferay_theme["wrap-portlet"] page="portlet.ftl">
				<@liferay_util["include"] page=content_include />
			</@>
		</#if>
	</section>

	<footer id="footer" role="contentinfo" style="margin-left:0px;">
		<!-- <p class="powered-by">
			<@liferay.language key="powered-by" /> <a href="http://www.liferay.com" rel="external">Liferay</a>
		</p> -->
	
<footer id="footer" role="contentinfo" style="margin-left:0px;">
    <div class="container-fluid" style=" background-color:#262626;text-align:left">
   
    <div class="col-md-3">
        <ul style="list-style-type:none">
        <h4 style="color: #1089b5">Get In Touch</h4>
        <li class="pow" >22/1,2nd Floor Madeeha Square,</li>
        <li class="pow">Coles Road, Frazer Town,</li>
        <li class="pow">Bengaluru-560005.</li>
        <li class="pow">Karnataka, India.</li>
        <li class="pow">Ph:080-65000846</li>
        <li class="pow">Email:sales@esquareinfo.com</li>
        </ul>
        </div>
   
       
    <div class="col-md-3">
        <ul style="list-style-type:none">
        <h4 style="color: #1089b5">Informations</h4>
        <li class="pow"><a href="/employee-information" rel="external" style= "color:white; text-decoration:none;">Employee Information</a></li>
        <li class="pow"><a href="/joinees-information" rel="external" style= "color:white; text-decoration:none;">Joinees Information</a></li>
        
       
        </ul>
        </div>
       
       
    <div class="col-md-3">
        <ul style="list-style-type:none">
        <h4 style="color: #1089b5">policies</h4>
        <li class="pow"><a href="/leave-policy" rel="external" style= "color:white; text-decoration:none;">Leave Policy</a></li>
        <li class="pow"><a href="/leave-calender" rel="external" style= "color:white; text-decoration:none;">Leave Calender</a></li>
       
        </ul>
        </div>
       
       
    <div class="col-md-3">
        <ul style="list-style-type:none">
        <h4 style="color: #1089b5">Help</h4>
        <li class="pow"><a href="/help-desk" rel="external" style= "color:white; text-decoration:none;">Help Desk</a></li>
        <li class="pow"><a href="/announcement" rel="external" style= "color:white; text-decoration:none;">Announcement</a></li>
        
        </ul>
       
        </div>
       
        
        </div>
        
       
       
        <div class="container-fluid" style="background-color: black; text-align: center; margin-left: -1px;">
       
      
        <div class="span2" style="color:#FFA500;">
        <p class="fot">2017 LinkedIn Corporation</p>
        </div>
        <div class="span2" style="color:#FFA500;>
        <p class="fot">Site Map</p>
        </div>
        <div class="span2" style="color:#FFA500;>
        <p class="fot">Privacy Policy</p>
        </div>
        <div class="span4" style="color:#FFA500;>
        <p class="fot">Web Use Policy</>
        </div>
        <div class="span2">
        <h3 class="fot" style="margin-top: 0px;color: #DCDCDC">esquareinfo.com</h3>
        
        </div>
        </div>
		
	</footer>
</footer>
</div>

<@liferay_util["include"] page=body_bottom_include />

<@liferay_util["include"] page=bottom_include />

</body>

</html>

<style>
.portlet-title-text{
	display:none!important;
}
#footer{
    Backgrount-color:#32363d!important;
      margin-left: 0px!important;
    margin-top:-10px!important;
}

li{
    color:white;
}
@media only screen and (min-width:300px) and (max-width: 450px) {
    .col-md-3 {width:100%;padding-left: 120px;}}
</style>